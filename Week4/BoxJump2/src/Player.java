import java.awt.*;

public class Player{

    private static final int PLAYER_WIDTH = 20;
    private static final int PLAYER_HEIGHT = 20;
    private static final int MAX_VELOCITY_RIGHT = 4;
    private static final double GRAVITY = 0.6;
    private static final int VERTICAL_LEAP = -7;
    private GamePanel panel;
    int x, y, width, height;
    double xspeed, yspeed;
    Rectangle collisionBox;
    boolean keyUp;
    boolean jumping = false;


    public Player(int x, int y, GamePanel panel){
        this.panel = panel; //access objects within GamePanel
        this.x = x;
        this.y = y;
        width = PLAYER_WIDTH;
        height = PLAYER_HEIGHT;

        collisionBox = new Rectangle(x,y,width,height); //player's hit collision box
    }

    public void set(){
        //System.out.println(y);
        if(panel.gameStart){
            xspeed++;
            panel.startActivated = true;
        }

        if(xspeed > MAX_VELOCITY_RIGHT) //cap the max velocity to the right
            xspeed = MAX_VELOCITY_RIGHT;

        if(keyUp && panel.startActivated){
            collisionBox.y++; //purposely bring the player one pixel down for collision check
            jumping = true;

            for(Floor floor: panel.floor){
                if(floor.collisionBox.intersects(collisionBox)) { //check if touching ground, enable jump
                    yspeed = VERTICAL_LEAP;
                }
            }
            collisionBox.y--; //prevent the player from being off-alignment
        }

        yspeed += GRAVITY; //gravity
        collisionBox.x += xspeed;
        collisionBox.y += yspeed;

        //floor collision
        for(Floor floor: panel.floor){
            if(collisionBox.intersects(floor.collisionBox)){
                collisionBox.y -= yspeed;
                while(!floor.collisionBox.intersects(collisionBox))
                    collisionBox.y += Math.signum(yspeed);
                yspeed = 0;
                y = collisionBox.y;
            }
        }

        //wall collision
        for(Wall wall: panel.walls){
            if(collisionBox.intersects(wall.collisionBoxWall)){
                //xspeed = 0;
                panel.DEATH_COUNT++;
                panel.resetCurrLVL();
            }
        }

        x += xspeed; //player movement
        y += yspeed;
        collisionBox.x = x; //follow player.x
        collisionBox.y = y; //follow player.y

        //go to next level when player has reached the end of the platform
        if(x > panel.END_OF_FLOOR){
            panel.LEVEL++;
            panel.reset();
        }
    }

    public void draw(Graphics2D g2d){
        g2d.setColor(Color.ORANGE);
        g2d.fillRect(x,y,width,height);
    }
}
