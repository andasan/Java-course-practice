import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.*;

public class GamePanel extends javax.swing.JPanel{

    /*
    DEVELOPER MODE?
     */
    boolean developerMode = false;

    private static final int PLAYER_SPAWN_X = 100;
    private static final int PLAYER_SPAWN_Y = 300;
    private static final int INTERVAL = 17; //wait 17ms between every single frame (1000ms/60fps = 16.666666)
    private static final int FLOOR_S = 20;
    private static final int FLOOR_L = 25;
    //private static final int WALL_S1 = 20;
    public final int END_OF_FLOOR = 580;
    public int DEATH_COUNT=0;
    public int LEVEL=1;

    boolean gameStart = false;
    boolean startActivated = false;

    Player player;
    ArrayList<Floor> floor = new ArrayList<>();
    ArrayList<Wall> walls = new ArrayList<>();
    Timer gameTimer;


    public GamePanel(){
        player = new Player(PLAYER_SPAWN_X,PLAYER_SPAWN_Y, this);
        reset();
        gameTimer = new Timer();
        gameTimer.schedule(new TimerTask() {
            @Override
            public void run() { //run every single frame

                player.set(); //update player's position based on keyinput
                repaint();
            }
        }, 0,INTERVAL);
    }

    public void reset(){
        player.x = PLAYER_SPAWN_X;
        player.y = PLAYER_SPAWN_Y;
        player.xspeed = 0;
        player.yspeed = 0;
        floor.clear();
        walls.clear();
        createFloor();
        createWall();
    }

    public void resetCurrLVL(){
        player.x = PLAYER_SPAWN_X;
        player.y = PLAYER_SPAWN_Y;
        player.xspeed = 0;
        player.yspeed = 0;
    }

    public void createFloor(){
        for(int i = 0; i< FLOOR_L; i++) floor.add(new Floor(Color.WHITE, PLAYER_SPAWN_X + i* FLOOR_S, PLAYER_SPAWN_Y+player.height, FLOOR_S, FLOOR_S));
    }

    public void createWall(){

        if(developerMode && LEVEL <= 10){
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 225, PLAYER_SPAWN_Y-1, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 315, PLAYER_SPAWN_Y-1, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 410, PLAYER_SPAWN_Y-1, 20, 20));
        }
        else if(LEVEL > 10){
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+20, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+40, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+60, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+80, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+100, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+120, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+140, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+160, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+180, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+200, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+220, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+240, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+260, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+280, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+300, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+320, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+340, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+360, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+380, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+400, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+420, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+440, PLAYER_SPAWN_Y-80, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+460, PLAYER_SPAWN_Y-60, 20, 20));
            walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X+480, PLAYER_SPAWN_Y-80, 20, 20));
        }
        else{
            if(LEVEL == 1){
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 280, PLAYER_SPAWN_Y-5, 20, 30));
            }
            else if(LEVEL == 2){
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 280, PLAYER_SPAWN_Y-5, 20, 30));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 400, PLAYER_SPAWN_Y-10, 20, 30));
            }
            else if(LEVEL == 3){
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 120, PLAYER_SPAWN_Y-5, 20, 25));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 280, PLAYER_SPAWN_Y-10, 20, 30));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 400, PLAYER_SPAWN_Y-10, 20, 30));
            }
            else if(LEVEL == 4){
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 120, PLAYER_SPAWN_Y-5, 40, 3)); //overhead
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 220, PLAYER_SPAWN_Y+10, 40, 10));
            }
            else if(LEVEL == 5){
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 240, PLAYER_SPAWN_Y-3, 25, 22));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 220, PLAYER_SPAWN_Y+2, 20, 18));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 120, PLAYER_SPAWN_Y-5, 40, 3));
            }
            else if(LEVEL == 6){
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 210, PLAYER_SPAWN_Y-3, 25, 22));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 280, PLAYER_SPAWN_Y-5, 40, 3));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 360, PLAYER_SPAWN_Y-3, 25, 22));
            }
            else if(LEVEL == 7){
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 150, PLAYER_SPAWN_Y-10, 20, 30));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 280, PLAYER_SPAWN_Y-10, 20, 30));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 480, PLAYER_SPAWN_Y-10, 20, 30));
            }
            else if(LEVEL == 8){
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 140, PLAYER_SPAWN_Y-1, 20, 20));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 150, PLAYER_SPAWN_Y-10, 20, 30));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 220, PLAYER_SPAWN_Y-5, 200, 3));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 470, PLAYER_SPAWN_Y-1, 20, 20));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 480, PLAYER_SPAWN_Y-10, 20, 30));
            }
            else if(LEVEL == 9){
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 140, PLAYER_SPAWN_Y-1, 20, 20));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 225, PLAYER_SPAWN_Y-1, 20, 20));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 315, PLAYER_SPAWN_Y-1, 20, 20));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 410, PLAYER_SPAWN_Y-1, 20, 20));
            }
            else if(LEVEL == 10) { //HARD!!!
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 130, PLAYER_SPAWN_Y-1, 20, 20));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 150, PLAYER_SPAWN_Y-10, 20, 30));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 260, PLAYER_SPAWN_Y-1, 20, 20));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 280, PLAYER_SPAWN_Y-10, 20, 30));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 460, PLAYER_SPAWN_Y-1, 20, 20));
                walls.add(new Wall(Color.WHITE, PLAYER_SPAWN_X + 480, PLAYER_SPAWN_Y-10, 20, 30));
            }
        }
    }

    public void paint(Graphics g){
        super.paint(g); //paint over the previous frame to prevent flickering

        Graphics2D g2d = (Graphics2D) g; //cast Graphics g to Graphics2D
        player.draw(g2d); //draw the player

        for(Floor floor: floor)
            floor.draw(g2d);

        for(Wall wall: walls)
            wall.draw(g2d);

        g2d.setColor(Color.BLACK);
        Font f = new Font("Monospaced",Font.BOLD,20);
        g2d.setFont(f);
        g2d.drawString("DEATH:",100,80);
        g2d.drawString(Integer.toString(DEATH_COUNT), 180, 80);
        g2d.drawString("LEVEL:",100,60);
        if(LEVEL <=10) {
            g2d.drawString(Integer.toString(LEVEL), 180, 60);
        }

        

        g2d.setColor(Color.WHITE);
        Font f2 = new Font("Monospaced",Font.BOLD,10);
        g2d.setFont(f2);
        g2d.drawString("Press space to start/jump:",700/2-73,500);
        g2d.drawString("Press R to reset:",700/2-50,520);
        

        if(developerMode && LEVEL <= 10) {
            g2d.setColor(Color.WHITE);
            Font f3 = new Font("Monospaced", Font.BOLD, 30);
            g2d.setFont(f3);
            g2d.drawString("DEVELOPER MODE", 220, 200);
        }
        if(!developerMode && LEVEL <= 10){
            g2d.setColor(Color.WHITE);
            Font f3 = new Font("Comic Sans MS", Font.BOLD, 50);
            g2d.setFont(f3);
            g2d.drawString("BOX JUMP", 210, 200);
        }
        if(LEVEL > 10){
            g2d.setColor(Color.WHITE);
            Font f3 = new Font("Comic Sans MS",Font.BOLD,50);
            g2d.setFont(f3);
            g2d.drawString("COMPLETED!!!",180,170);
        }

    }

    void keyPressed(KeyEvent e){
        if(e.getKeyCode()== KeyEvent.VK_SPACE)
            player.keyUp = true;
        if(e.getKeyCode() == KeyEvent.VK_R){
            LEVEL = 1;
            gameStart = false;
            startActivated = false;
            reset();
        }
    }

    void keyReleased(KeyEvent e){
        if(e.getKeyCode()== KeyEvent.VK_SPACE) {
            player.keyUp = false;
            gameStart = true;
        }
    }

    public void mouseClicked(MouseEvent e) {
    }
}
