import javax.swing.*;
import java.awt.*;

public class BoxJump{
    public static final int FRAME_WIDTH = 700;
    public static final int FRAME_HEIGHT = 700;

    public static void main(String[] args) {
        MainFrame frame = new MainFrame();
        frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);

        //centralize window in your desktop screen
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int cenW = (int)(screenSize.getWidth()/2 - frame.getSize().getWidth()/2);
        int cenH = (int)(screenSize.getHeight()/2 - frame.getSize().getHeight()/2);
        frame.setLocation(cenW,cenH);

        frame.setResizable(false);
        frame.setTitle("Box Jump");
        frame.setVisible(true);

        //frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
