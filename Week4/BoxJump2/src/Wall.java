import java.awt.*;

public class Wall {
    public int x, y, width, height;
    Rectangle collisionBoxWall;
    Color color;

    public Wall(Color color, int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;

        collisionBoxWall = new Rectangle(x,y, width,height);
    }

    public void draw(Graphics2D g) {
        g.setColor(Color.WHITE);
        g.drawRect(x,y,width,height);
        g.setColor(color);
        g.fillRect(x,y,width,height);
    }
}