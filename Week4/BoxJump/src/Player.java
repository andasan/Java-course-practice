import java.awt.*;

public class Player {

    private static final int PLAYER_WIDTH = 50;
    private static final int PLAYER_HEIGHT = 50;
    private static final double MIN_VELOCITY_LEFT = -0.75;
    private static final double MIN_VELOCITY_RIGHT = 0.75;
    private static final int MAX_VELOCITY_LEFT = -7;
    private static final int MAX_VELOCITY_RIGHT = 7;
    private static final double GRAVITY = 0.7;
    private static final int VERTICAL_LEAP = -12;
    private static final int MAX_JUMPS = 2;
    private GamePanel panel;
    BoxJump boxJump;
    int x, y, width, height;
    double xspeed, yspeed;
    Rectangle collisionBox;
    boolean keyLeft,keyRight,keyUp;

    int jumps = 0;

    public Player(int x, int y, GamePanel panel){
        this.panel = panel; //access objects within GamePanel
        this.x = x;
        this.y = y;
        width = PLAYER_WIDTH;
        height = PLAYER_HEIGHT;

        collisionBox = new Rectangle(x,y,width,height); //player's hit collision box
    }

    public void set(){
        if(keyLeft && keyRight || !keyLeft && !keyRight)
            xspeed *= 0.8; //decrease the speed smoothly if we're not pressing anything
        else if(keyLeft && !keyRight)
            xspeed--; //move the player to the left
        else if(keyRight && !keyLeft)
            xspeed++; //move the player to the right

        //prevent the player from sliding off when key is released due to the multiplier of xspeed
        if(xspeed > 0 && xspeed < MIN_VELOCITY_RIGHT) //between 0 velocity and MIN velocity to the right direction, make the player stop
            xspeed = 0;
        if(xspeed < 0 && xspeed > MIN_VELOCITY_LEFT) //between 0 velocity and MIN velocity to the left direction, make the player stop
            xspeed = 0;
        if(xspeed > MAX_VELOCITY_RIGHT) //cap the max velocity to the right
            xspeed = MAX_VELOCITY_RIGHT;
        if(xspeed < MAX_VELOCITY_LEFT) //cap the max velocity to the left
            xspeed = MAX_VELOCITY_LEFT;

        if(keyUp){
            collisionBox.y++;
            for(Wall wall: panel.walls){
                if(wall.hitBox.intersects(collisionBox)) { //check if touching ground, enable jump
                    yspeed = VERTICAL_LEAP;
                }
                if(jumps < MAX_JUMPS) {
                    //yspeed = VERTICAL_LEAP;
                }

            }
            collisionBox.y--;
        }

        yspeed += GRAVITY; //gravity

        //horizontal collision
        collisionBox.x += xspeed;
        for(Wall wall: panel.walls){
            if(collisionBox.intersects(wall.hitBox)){
                collisionBox.x -= xspeed;
                while(!wall.hitBox.intersects(collisionBox))
                    collisionBox.x += Math.signum(xspeed);
                collisionBox.x -= Math.signum(xspeed);
                panel.cameraX += x- collisionBox.x;
                xspeed = 0;
                collisionBox.x = x;
            }
        }

        //vertical collision
        collisionBox.y += yspeed;
        for(Wall wall: panel.walls){
            if(collisionBox.intersects(wall.hitBox)){
                collisionBox.y -= yspeed;
                while(!wall.hitBox.intersects(collisionBox))
                    collisionBox.y += Math.signum(yspeed);
                collisionBox.y -= Math.signum(yspeed);
                yspeed = 0;
                jumps = 0;
                y = collisionBox.y;
            }
        }

        //x += xspeed; //player movement
        panel.cameraX -= xspeed; //apply movement to camera
        y += yspeed;
        collisionBox.x = x; //follow player.x
        collisionBox.y = y; //follow player.y

        //kill a player from pitfall
        if(y > boxJump.FRAME_HEIGHT)
            panel.reset();
    }

    public void draw(Graphics2D g2d){
        g2d.setColor(Color.BLACK);
        g2d.fillRect(x,y,width,height);
    }
}
