import java.awt.*;

public class MainFrame extends javax.swing.JFrame {

    public MainFrame(){
        GamePanel panel = new GamePanel();
        panel.setLocation(0,0);
        panel.setSize(this.getSize()); //same size as frame
        panel.setBackground(Color.DARK_GRAY);
        panel.setVisible(true);
        this.add(panel);

        addKeyListener(new KeyInputs(panel));
        addMouseListener(new MouseClick(panel));
    }
}
