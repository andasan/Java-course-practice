import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.*;
public class GamePanel extends javax.swing.JPanel{

    private static final int PLAYER_SPAWN_X = 100;
    private static final int PLAYER_SPAWN_Y = 300;
    private static final int INTERVAL = 17; //wait 17ms between every single frame (1000ms/60fps = 16.666666)
    private static final int WALL_S = 50;
    private static final int SCREEN_OFFSET = 100;
    private static final int OFFSET_WALL = -150;
    public int HIGH_SCORE=0;

    int cameraX;
    int offset;
    boolean gameStart = false;
    Rectangle startButton;
    Font btnFont = new Font("Arial",Font.BOLD,30);
    Player player;
    BoxJump boxJump;
    ArrayList<Wall> walls = new ArrayList<>();
    Timer gameTimer;

    public GamePanel(){
        startButton = new Rectangle(550, 25,100,50);
        player = new Player(PLAYER_SPAWN_X,PLAYER_SPAWN_Y, this);
        reset();
        gameTimer = new Timer();
        gameTimer.schedule(new TimerTask() {
            @Override
            public void run() { //run every single frame

                //generate pre-made platform before it shows on the screen
                if(walls.get(walls.size()-1).x < boxJump.FRAME_WIDTH+SCREEN_OFFSET){
                    offset+=boxJump.FRAME_WIDTH;
                    createWall(offset);
                }
                if(gameStart){
                    player.set(); //update player's position based on keyinput
                    if(HIGH_SCORE > cameraX) HIGH_SCORE=cameraX;
                }

                for(Wall wall: walls)
                    wall.set(cameraX);

                //remove created platforms that have passed the screen to save memory
                for(int i=0; i<walls.size(); i++){
                    if(walls.get(i).x < -800)
                        walls.remove(i);
                }
                repaint();
            }
        }, 0,INTERVAL);
    }

    public void reset(){
        player.x = PLAYER_SPAWN_X;
        player.y = PLAYER_SPAWN_Y;
        cameraX = PLAYER_SPAWN_X;
        player.xspeed = 0;
        player.yspeed = 0;
        walls.clear();
        offset = OFFSET_WALL;

        createWall(offset);
    }

    public void createWall(int offset){
        Random r = new Random();
        int index = r.nextInt(8);
        if(index == 0){
            for(int i=0; i<14; i++) walls.add(new Wall(Color.PINK, offset + i*50, 600, WALL_S, WALL_S));
        }

        else if(index == 1){
            for(int i=0; i<5; i++) walls.add(new Wall(Color.WHITE, offset + i*50, 600, WALL_S, WALL_S));
            walls.add(new Wall(Color.WHITE, offset + 500, 600, WALL_S, WALL_S));
            walls.add(new Wall(Color.WHITE, offset + 550, 600, WALL_S, WALL_S));
            walls.add(new Wall(Color.WHITE, offset + 600, 600, WALL_S, WALL_S));
            walls.add(new Wall(Color.WHITE, offset + 650, 600, WALL_S, WALL_S));
            walls.add(new Wall(Color.WHITE, offset + 700, 600, WALL_S, WALL_S));
            walls.add(new Wall(Color.WHITE, offset + 750, 600, WALL_S, WALL_S));
        }
        else if(index == 2){
            for(int i=0; i<14; i++) walls.add(new Wall(Color.RED, offset + i*50, 600, WALL_S, WALL_S));
            for(int i =0; i<12; i++) walls.add(new Wall(Color.RED, offset + 50 + i*50, 550, WALL_S, WALL_S));
            for(int i =0; i<10; i++) walls.add(new Wall(Color.RED, offset + 100 + i*50, 500, WALL_S, WALL_S));
            for(int i=0; i<8; i++) walls.add(new Wall(Color.RED, offset + 150 + i*50, 450, WALL_S, WALL_S));
            for(int i =0; i<6; i++) walls.add(new Wall(Color.RED, offset + 200 + i*50, 400, WALL_S, WALL_S));
        }
        else if(index == 3){
            for(int i=0; i<5; i++) walls.add(new Wall(Color.GREEN, offset + i*50, 600, WALL_S, WALL_S));
            for(int i=0; i<5; i++) walls.add(new Wall(Color.GREEN, offset + 450 + i*50, 600, WALL_S, WALL_S));
            walls.add(new Wall(Color.GREEN, offset + 150, 550, WALL_S, WALL_S));
            walls.add(new Wall(Color.GREEN, offset + 200, 550, WALL_S, WALL_S));
            walls.add(new Wall(Color.GREEN, offset + 200, 500, WALL_S, WALL_S));
            walls.add(new Wall(Color.GREEN, offset + 200, 450, WALL_S, WALL_S));
            walls.add(new Wall(Color.GREEN, offset + 500, 550, WALL_S, WALL_S));
            walls.add(new Wall(Color.GREEN, offset + 450, 550, WALL_S, WALL_S));
            walls.add(new Wall(Color.GREEN, offset + 450, 500, WALL_S, WALL_S));
            walls.add(new Wall(Color.GREEN, offset + 450, 450, WALL_S, WALL_S));
        }
        else if (index == 4){
            for(int i=0; i<5; i++) walls.add(new Wall(Color.BLUE, offset + i*50, 600, WALL_S, WALL_S));
            for(int i =0; i<4; i++) walls.add(new Wall(Color.BLUE, offset + 50 + i*50, 550, WALL_S, WALL_S));
            for(int i =0; i<3; i++) walls.add(new Wall(Color.BLUE, offset + 100 + i*50, 500, WALL_S, WALL_S));
            for(int i=0; i<2; i++) walls.add(new Wall(Color.BLUE, offset + 150 + i*50, 450, WALL_S, WALL_S));
            for(int i =0; i<4; i++) walls.add(new Wall(Color.BLUE, offset + 500 + i*50, 600, WALL_S, WALL_S));
        }
        else if (index ==5){
            for(int i=0; i<5; i++) walls.add(new Wall(Color.YELLOW, offset + i*50, 600, WALL_S, WALL_S));
            for(int i=0; i<3; i++) walls.add(new Wall(Color.YELLOW, offset + 100 + i*50, 550, WALL_S, WALL_S));
            for(int i=0; i<2; i++) walls.add(new Wall(Color.YELLOW, offset + 150 + i*50, 500, WALL_S, WALL_S));
            for(int j=0; j<4; j++){
                for(int i=0; i<4; i++) walls.add(new Wall(Color.YELLOW, offset + 350 + i*50, 400 + 50*j, WALL_S, WALL_S));
            }
            for(int i=0; i<7; i++) walls.add(new Wall(Color.YELLOW, offset + 350 + i*50, 600, WALL_S, WALL_S));
            for(int i=0; i<2; i++) walls.add(new Wall(Color.YELLOW, offset + 550, 500 + i*50, WALL_S, WALL_S));
        }
        else if (index == 6){
            for(int i=0; i<14; i++) walls.add(new Wall(Color.CYAN, offset + i*50, 600, WALL_S, WALL_S));
            for(int i=0; i<7; i++) walls.add(new Wall(Color.CYAN, offset + 200 + i*50, 450, WALL_S, WALL_S));
        }
        else if(index == 7){
            walls.add(new Wall(Color.MAGENTA, offset, 600, WALL_S, WALL_S));
            walls.add(new Wall(Color.MAGENTA, offset + 50, 600, WALL_S, WALL_S));

            walls.add(new Wall(Color.MAGENTA, offset + 150, 500, WALL_S, WALL_S));
            walls.add(new Wall(Color.MAGENTA, offset + 200, 500, WALL_S, WALL_S));

            walls.add(new Wall(Color.MAGENTA, offset + 300, 400, WALL_S, WALL_S));
            walls.add(new Wall(Color.MAGENTA, offset + 350, 400, WALL_S, WALL_S));

            walls.add(new Wall(Color.MAGENTA, offset + 450, 500, WALL_S, WALL_S));
            walls.add(new Wall(Color.MAGENTA, offset + 500, 500, WALL_S, WALL_S));

            walls.add(new Wall(Color.MAGENTA, offset + 600, 600, WALL_S, WALL_S));
            walls.add(new Wall(Color.MAGENTA, offset + 650, 600, WALL_S, WALL_S));
        }
    }

    public void paint(Graphics g){
        super.paint(g); //paint over the previous frame to prevent flickering

        Graphics2D g2d = (Graphics2D) g; //cast Graphics g to Graphics2D
        player.draw(g2d); //draw the player
        for(Wall wall: walls)
            wall.draw(g2d);

        g2d.setColor(Color.GREEN);
        g2d.drawRect(550,25,100,50);
        g2d.setColor(Color.WHITE);
        g2d.fillRect(551,26,98,48);
        g2d.setColor(Color.BLACK);
        g2d.setFont(btnFont);
        g2d.drawString("Start",564,60);

        g2d.setColor(Color.BLACK);
        Font f = new Font("Arial",Font.BOLD,20);
        g2d.setFont(f);
        g2d.drawString("HIGH SCORE:",100,80);
        g2d.drawString(Integer.toString(HIGH_SCORE*-1+100), 100, 100);
    }

    void keyPressed(KeyEvent e){
        if(e.getKeyCode() == KeyEvent.VK_LEFT)
            player.keyLeft = true;
        if(e.getKeyCode()== KeyEvent.VK_SPACE)
            player.keyUp = true;
        if(e.getKeyCode() == KeyEvent.VK_RIGHT)
            player.keyRight = true;
    }

    void keyReleased(KeyEvent e){
        if(e.getKeyCode() == KeyEvent.VK_LEFT)
            player.keyLeft = false;
        if(e.getKeyCode()== KeyEvent.VK_SPACE)
            player.keyUp = false;
            player.jumps+=1;
        if(e.getKeyCode() == KeyEvent.VK_RIGHT)
            player.keyRight = false;
    }

    public void mouseClicked(MouseEvent e) {
        if(startButton.contains(new Point(e.getPoint().x, e.getPoint().y -27))) {
            gameStart = true;
        }
    }
}
