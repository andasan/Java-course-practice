import java.awt.*;

public class Wall {
    public int x, y, width, height, startX;
    Rectangle hitBox;
    Color color;

    public Wall(Color color, int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        startX = x;
        this.width = width;
        this.height = height;
        this.color = color;

        hitBox = new Rectangle(x,y, width,height);
    }

    public void draw(Graphics2D g) {
        g.setColor(Color.BLACK);
        g.drawRect(x,y,width,height);
        g.setColor(color);
        g.fillRect(x+1,y+1,width-2,height-2);
    }

    public int set(int cameraX){
        x = startX + cameraX;
        hitBox.x = x;
        return x;
    }
}
