import java.awt.*;
import java.awt.event.*;
public class WindowLogin extends WindowAdapter{
    Frame f;
    WindowLogin(){
        f=new Frame();
        f.addWindowListener(this);

        TextField t1,t2;
        t1=new TextField();
        t1.setBounds(50,180, 300,30);
        t2=new TextField();
        t2.setEchoChar('*');
        t2.setBounds(50,250, 300,30);
        f.add(t1); f.add(t2);

        int width = 400;
        int height = 400;
        f.setSize(width, height);

        Label l0,l1,l2;
        l0=new Label("-----Please login-----");
        l1=new Label("Username");
        l2=new Label("Password");
        l0.setBounds(width/2-70,50,300,30);
        l1.setBounds(50,150,100,30);
        l2.setBounds(50,220, 100,30);
        f.add(l0); f.add(l1); f.add(l2);



        f.setLayout(null);
        f.setVisible(true);
    }
    public void windowClosing(WindowEvent e) {
        f.dispose();
    }
    public static void main(String[] args) {
        new WindowLogin();
    }
}