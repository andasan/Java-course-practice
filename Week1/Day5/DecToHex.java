import java.util.Scanner;

public class DecToHex {
    /** Main method */
    public static void main(String[] args) {
        // Create a Scanner
        Scanner input = new Scanner(System.in);

        // Prompt the user to enter a decimal integer
        System.out.print("Enter a decimal number: ");
        int decimal = input.nextInt();

        input.close();

        // Convert decimal to hex
        String hex = "";

        while (decimal != 0) {
            int hexValue = decimal % 16;

            // Convert a decimal value to a hex digit
            char hexDigit = (hexValue <= 9 && hexValue >= 0) ? (char)(hexValue + '0') : (char)(hexValue - 10 + 'A');
            System.out.println(hexDigit);
            hex = hexDigit + hex; //understanding string value
            System.out.println("hex: " +hex);
            decimal = decimal / 16;
        }

        System.out.println("The hex number is " + hex);
    }
}
