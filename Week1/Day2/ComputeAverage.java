/**
 * ComputeAverage
 */
import java.util.Scanner; //package
//import java.util.*; //global java packages
public class ComputeAverage {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // define input as a new object | (System.in) = computer keyboard input

        System.out.println("Enter three numbers.");
        double num1 = input.nextDouble();
        double num2 = input.nextDouble();
        double num3 = input.nextDouble();

        double average = (num1 + num2 + num3)/3;

        System.out.println(average);

        input.close();
    }
}