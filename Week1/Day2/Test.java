/**
 * Test
 */
// import java.util.Formatter;
 public class Test {

    public static void main(String[] args) {
        // double result = Math.pow(8,2.35);

        // Formatter formatter = new Formatter(); 
 
        // // format() has the same syntax as printf()
        // formatter.format("%.4f", result);     // 4 decimal places
        // System.out.println("Value of 8^2.35 : " + formatter.toString());
        // formatter.close();

        // //System.out.println();

        // double tax = 197.55 * 0.06;
        // System.out.println((int)(tax *100)/100.0); //divide to get the double back

        int number = 30;

        if (number % 2 == 0)
            System.out.println(number + " is even //");
        if (number % 5 == 0)
            System.out.println(number + " is multiple of 5 //");

        if (number % 2 == 0){
            System.out.println(number + " is even");
        }
        else if (number % 5 == 0){
            System.out.println(number + " is multiple of 5");
        }
        
        //with elseif condition there's no secondary checking if a number can be both even and a multiple of 5
    }
}