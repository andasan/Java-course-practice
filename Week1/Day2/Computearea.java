/**
 * Computearea
 */

import java.util.Scanner;  // Import the Scanner class
public class Computearea {

    public static void main(String[] args) {
        
        double area;
        double radius;

        Scanner scRad = new Scanner(System.in); 
        System.out.println("Enter radius");

        radius = scRad.nextDouble();  // Read user input of radius
        area = radius * radius * Math.PI;
        System.out.println("Area is: " +area);


        scRad.close();


    }
}