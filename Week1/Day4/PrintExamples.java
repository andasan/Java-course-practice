/**
 * PrintExamples
 */
public class PrintExamples {

    public static void main(String[] args) {

        System.out.printf("String is '%5s'\n", "A"); 
        System.out.printf("String is '%-5s'\n", "A"); 
        System.out.printf("String is '%.5s'\n", "Happy Birthday!"); 

        System.out.printf("String is '%5s'\n", "ABCDEFGHIJKL"); 

        
    }
}