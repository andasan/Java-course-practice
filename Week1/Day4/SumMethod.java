/**
 * SumMethod
 */
import java.util.Scanner;
 public class SumMethod {

    public static int sum(int i1, int i2){
        int result = 0;
        for(int i = 0; i <= i2; i++){
            result += i;
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter 2 numbers");
        int a1 = input.nextInt();
        int a2 = input.nextInt();
        System.out.println("Sum " +a1+ " to "+a2+" is: " +sum(a1,a2));
        // System.out.println("Sum " +a1+ " to '+a2+" is: " +sum(20,37));
        // System.out.println("Sum 35 to 49 is: " +sum(35,49));
        
    }
}