/**
 * Test
 */
public class Test {

    public static void main(String[] args) {
        // System.out.println(Math.exp(1));
        // System.out.println(Math.log(Math.E));
        // System.out.println(Math.log10(10));
        // System.out.println(Math.pow(2, 3));
        // System.out.println(Math.pow(3, 2));
        // System.out.println(Math.pow(4.5, 2.5));
        // System.out.println(Math.sqrt(4));
        // System.out.println(Math.sqrt(10.5));

        //charAt
        String s = "Strings are immutable"; 
        char result = s.charAt(8); 
        System.out.println(result); 


        System.out.println(increase(1));

        //Generate random int and char
        int rand = (int)((int)'a' + Math.random() * ((int)'z' - (int)'a' + 1));
        System.out.println(rand);

        char rand2 = (char)('a' + Math.random() * ('z' - 'a' + 1));
        System.out.println(rand2);

        //random generator with specific range
        //Min + (int)(Math.random() * ((Max - Min) + 1))
        //(int)(Min + Math.random() * ((Max - Min) + 1))

    }

    public static int increase(int n){
        n++;
        return n;
    }
}