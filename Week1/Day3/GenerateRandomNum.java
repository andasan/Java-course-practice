import java.util.Random;

/**
 * GenerateRandomNum
 */
public class GenerateRandomNum {

    public static void main(String[] args) {
        int randomNum = (int)(Math.random());
        
        do{
            randomNum = (int)(Math.random());
        }
        while(randomNum <= 0 && randomNum < 20);
        
        System.out.println(randomNum);
        
    }
}