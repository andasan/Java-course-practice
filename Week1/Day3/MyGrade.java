/**
 * MyGrade
 */
import java.util.Scanner;
public class MyGrade {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("ENTER YOUR SCORES FOR MATH, SCI and ENG");
        //System.out.println("What's your score for Math");
        double math = input.nextDouble();
        //System.out.println("What's your score for Science");
        double science = input.nextDouble();
        //System.out.println("What's your score for English");
        double english = input.nextDouble();

        double average = (math + science + english)/3;
        

        if (average >= 90.0)
            System.out.println("A");
        else if (average >= 80.0)
            System.out.println("B");
        else if (average >= 70.0)
            System.out.println("C");
        else if (average >= 60.0)
            System.out.println("D");
        else
            System.out.println("F");


        input.close();
    }
}