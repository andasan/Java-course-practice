/**
 * About Substrings
 */
public class TestWoneDthree {

    public static void main(String[] args) {

        
        // String Str = new String("WELCOME TO JAVA COURSE");
        // System.out.print("Return Value: ");
        // System.out.println(Str.substring(11));

        // String str = "This is Java Computer Science A Lab";      
        // String substr = "";          
        // substr = str.substring(7, 21);  // prints the substring after index 7 till index 21  
        // System.out.println("substring = " + substr);      
        // substr = str.substring(0, 7);  // prints the substring after index 0 till index 7 
        // System.out.println("substring = " + substr);   

        //CompareTo method
        // String str1 = "New York";      
        // String str2 = new String("New York");      
        // String str3 = new String("Boston");            
        // int result = str1.compareTo( str2 );      
        // System.out.println(result);            
        // result = str2.compareTo( str3 );      
        // System.out.println(result);   

        // Integer x = 5;            
        // System.out.println(x.compareTo(3));      
        // System.out.println(x.compareTo(5));      
        // System.out.println(x.compareTo(8));  
        
        //indexOf
        // String strOriginal = "Hello readers";      
        // int search = strOriginal.indexOf("Hello");            
        // if(search == - 1) {         
        //     System.out.println("Hello not found");      
        // } 
        // else {         
        //     System.out.println("Found Hello at index " + search);     
        // } 


        //Contains method
        String str = "Seoul is the capital city of Korea";
        if(str.contains("city")){
            System.out.println("This string contains city");
        }
        else{
            System.out.println("Result not found");
        }


        //Trim method
        String Str = new String("    Welcome to Java Class.    ");
        System.out.print("Return Value : " );
        System.out.println(Str.trim() );



    }
}