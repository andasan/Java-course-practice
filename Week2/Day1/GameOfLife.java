// A simple Java program to implement Game of Life 
public class GameOfLife 
{ 
	public static void main(String[] args) 
	{ 
		int X = 10, Y = 10; 

		// Intiliazing the grid. 
		int[][] grid = { { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 
                         { 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 }, 
                         { 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 }, 
                         { 0, 0, 0, 0, 0, 1, 1, 0, 0, 0 }, 
                         { 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 }, 
                         { 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 }, 
                         { 0, 0, 1, 1, 0, 0, 0, 0, 0, 0 }, 
                         { 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 }, 
                         { 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 }, 
                         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } 
		}; 

		// Displaying the grid 
		System.out.println("Original Generation"); 
		for (int i = 0; i < X; i++) 
		{ 
			for (int j = 0; j < Y; j++) 
			{ 
				if (grid[i][j] == 0) 
					System.out.print("."); 
				else
					System.out.print("*"); 
			} 
			System.out.println(); 
		} 
		System.out.println(); 
		nextGen(grid, X, Y); 
	} 

	// Function to print next generation 
	static void nextGen(int grid[][], int X, int Y) 
	{ 
        int[][] future = new int[X][Y]; 
        boolean isAlive = false;
        boolean remainsTheSame = false;
        int counter = 0;

		// Loop through every cell 
		for (int newX = 1; newX < X - 1; newX++) 
		{ 
			for (int newY = 1; newY < Y - 1; newY++) 
			{ 
				// finding no Of Neighbours that are alive 
				int aliveNeighbours = 0; 
				for (int i = -1; i <= 1; i++) 
					for (int j = -1; j <= 1; j++) 
						aliveNeighbours += grid[newX + i][newY + j]; 

				// The cell needs to be subtracted from 
				// its neighbours as it was counted before 
				aliveNeighbours -= grid[newX][newY]; 

				// Implementing the Rules of Life 

				// Cell is lonely and dies 
				if ((grid[newX][newY] == 1) && (aliveNeighbours < 2)) 
					future[newX][newY] = 0; 

				// Cell dies due to over population 
				else if ((grid[newX][newY] == 1) && (aliveNeighbours > 3)) 
					future[newX][newY] = 0; 

				// A new cell is born 
				else if ((grid[newX][newY] == 0) && (aliveNeighbours == 3)) 
					future[newX][newY] = 1; 

				// Remains the same 
				else
                    future[newX][newY] = grid[newX][newY]; 
                    remainsTheSame = true;
			} 
		} 

		System.out.println("Next Generation"); 
		for (int i = 0; i < X; i++) 
		{ 
			for (int j = 0; j < Y; j++) 
			{ 
				if (future[i][j] == 0){
                    System.out.print(".");
                }
				else{
                    System.out.print("*"); 
                    isAlive = true; 
                    counter++;
                }
			} 
			System.out.println(); 
        }

        //System.out.println(counter);
        
        if(isAlive && !remainsTheSame){
            System.out.println("CONTINUE");
            //nextGen(grid, X, Y);
        }
        else if(remainsTheSame){
            System.out.println("ALL ARE DEAD");
        }
         
	} 
} 
