// Here, I create a class called DiceFace, it contains two variables
// int value, which will store a number, meant to represent the 'number of the face of a dice'
// and a String called color, which we just put in some text that says what color that face is
 class DiceFace
    {
    public int value = 0;
    public String color = "red";
    }
    
    
    
// Here I create an actual Dice
 class Dice
    {
        // a dice has some number of sides, so I use a variable to represent that, of type int
        int numOfSides = 0;
        // a dice also has the same number of 'faces/sides' that it does 'numbers', so we create
        // an array of faces on the dice and just imagine that it is real. Each face, because
        // it is created from 'DiceFace class' has a value and a color
        DiceFace[] faces;

       // does nothing,, just an example of a private method, ie: code that can't be seen outside of
       // the Dice class. 
        private int cheat()
        {
        return 0;
        }
        
        
        // here we have a function that rolls the dice
        public int RollTheDice()
            {
                // a roll of the dice involves a few steps. 
                /*
                   1. We need to get a random numbers
                   2. We need to make sure that number is not
                      out of bounds
                   3. using that nummber, we will use it to access 
                      one of the 'faces' we created earlier, because
                      the 'face' has a variable called 'value', which
                      is where the value of that roll is. 
                */
                int faceIWillLookAt = 0;
                
                // see: https://www.tutorialspoint.com/java/number_floor.htm
                // and - https://www.geeksforgeeks.org/java-math-random-method-examples/
                faceIWillLookAt = (int)Math.floor((Math.random() * faces.length));
                
                // now we have a number, we can access the array and the value
                // and we use 'return' to give it back to the user who called
                // this function
                return faces[faceIWillLookAt].value;
            }


       // this is our construvtor, it is called when we use the 'new' keyword in java
        public Dice(int sidesOnDice)
            {
            // lets see if our values are within range, and not to high or two low
            if ((sidesOnDice < 6) || (sidesOnDice > 128))
                {
                // show an error
                // you should tell the user he did something wrong. 
                }
            else
                { // else, if no error, then we do this
                  // create the FACES array
                    faces = new DiceFace[sidesOnDice];
                  // loop through the array and assign it a value,
                  // notice that we use i+1 here, not i. 
                    for (int i = 0; i < faces.length; i++)
                        {
                        faces[i].value = i+1;
                        }

                }


            }

    }


// here is our main program class. 
public class Main
    {
    // lets make some dice arrays
    public static Dice[] sixSidedDice;
    public static Dice[] twelveSidedDice;
    // and our constructor
    public static void main(String[] args)
        {
           // 6 sided dice/
            sixSidedDice = new Dice[100];
            for (int i = 0; i < sixSidedDice.length; i++)
                {// loop through each element in array, create the dice, and roll the dice
                sixSidedDice[i] = new Dice(6);
                sixSidedDice[i].RollTheDice();
                }
         // 12 sided dice. 
            twelveSidedDice = new Dice[100];
            for (int i = 0; i < twelveSidedDice.length; i++)
                {// loop through each element in array, create the dice, and roll the dice
                    twelveSidedDice[i] = new Dice(12);
                    twelveSidedDice[i].RollTheDice();
                }

            
        }
    }

// class DiceFace{
//     public int val = 0;
// }

// class Dice {
//     int numOfSides = 0;
//     DiceFace[] faces;

//     public int RollTheDice(){
//         int faceIWillLookAt = (int)(Math.random() * faces.length);
//         return faces[faceIWillLookAt].val;
//     }
//     public Dice(int sidesOnDice){
//         if(sidesOnDice < 6 || sidesOnDice > 128){
//             //show an error
//         }
//         else{
//             faces = new DiceFace[sidesOnDice];
//             for(int i = 0; i < sidesOnDice;i++){
//                 faces[i].val = i+1;
//             }
//         }
        
//     }
// }
// public class Main{
    
//     public static Dice[] sixSidedDice;
//     public static Dice[] twelveSidedDice;
//     public static void main(String[] args) {

//         sixSidedDice = new Dice[100];
//         for(int i = 0; i > sixSidedDice.length; i++){
//             sixSidedDice[i] = new Dice(6);
//         }

//         twelveSidedDice = new Dice[100];
//         for(int i = 0; i> twelveSidedDice.length; i++){
//             twelveSidedDice[i] = new Dice(12);
//         }

//         // sixSidedDice.RollTheDice();
//         // twelveSidedDice.RollTheDice();

//     }
// }