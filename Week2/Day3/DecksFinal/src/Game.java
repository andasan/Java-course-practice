import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Game {
    Deck d = new Deck();
    int n = d.ranks.length * d.suits.length;
    Card[] cards = new Card[n];
    ArrayList<String> cardList = new ArrayList<>();

    int counter = 1;
    int suitIndex = 0;
    int rankIndex = 0;

    public Game() {

        //Using ArrayList to contain cards' rank and suit
        for (int i = 0; i < d.ranks.length; i++) {
            for (int j = 0; j < d.suits.length; j++) {
                cardList.add(d.ranks[i]+" of "+d.suits[j]);
            }
        }

        //important! instantiate each element of cards
        for(int i=0; i< cards.length;i++)
            cards[i] = new Card();

        //Using Array type to contain cards' rank and suit
        for(int x = 0; x < n; x++){

            //this is a shortcut... rather than separating instantiation of card element and instantiation of card variables
            //cards[x] = new Card(d.ranks[rankIndex],d.suits[suitIndex]);

            //viable but takes up too much memory (2 for loops: instantiate card and card variables)
            cards[x].suit = d.suits[suitIndex];
            cards[x].rank = d.ranks[rankIndex];

            //note to myself: don't use this
            if(x%13 == 0 && x != 0){
                //suitIndex++;
            }

            //note to myself: use this
            if(x==(n/d.suits.length*counter-1)){
                suitIndex++;
                rankIndex = 0;
                counter++;
            }else{
                rankIndex++;
            }
        }
    }

    public int[] Game(int pick){
        Game game = new Game();
        int[] deckArr = new int[cards.length];
        int[] pickedArr = new int[pick];

        //init arr with card length values for each index
        for(int x=0; x<cards.length;x++){
            deckArr[x] = x;
        }

        for(int x=0; x<pick;x++){
            //get a random element
            int rand = game.getRandomElement(deckArr);
            //insert picked element into an array
            pickedArr[x] = rand;
            // Remove the element
            deckArr = removeTheElement(deckArr, rand);
        }
        return pickedArr;
    }

    // Function select an element base on index
    // and return an element
    public int getRandomElement(int[] list)
    {
        int rnd = new Random().nextInt(list.length);
        return list[rnd];
    }

    //
    public static int[] removeTheElement(int[] arr, int index) {

        if (arr == null || index < 0 || index >= arr.length)
            return arr;
        // Create ArrayList from the array
        List<Integer> arrayList = IntStream.of(arr).boxed().collect(Collectors.toList());

        // Remove the specified element
        arrayList.remove(index);

        return arrayList.stream().mapToInt(Integer::intValue).toArray();
    }
}
