import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Game game = new Game();

        //Display a deck of cards from ArrayList: cardList
        System.out.println("-------------ArrayList (arranged by rank)---------------");
        for (String newDeck : game.cardList) {
            System.out.print(newDeck+" | ");
        }
        System.out.println();

        //Shuffle the cards with Collection framework of shuffle method
        System.out.println("-------------SHUFFLE---------------");
        Collections.shuffle(game.cardList);
        for (String shuffledDeck : game.cardList) {
            System.out.print(shuffledDeck+" | ");
        }
        System.out.println();

        //Display a deck of cards from Array
        System.out.println("-------------Array (arranged by suits)---------------");
        Deck d = new Deck();
        int count = 1;
        for (int i = 0 ; i < game.cards.length; i++) {
            if(i<game.cards.length-1){
                System.out.print(game.cards[i].rank+ " of "+game.cards[i].suit+", ");
            }else{
                System.out.print(game.cards[i].rank+ " of "+game.cards[i].suit);
            }

            if(i==(game.cards.length/d.suits.length*count-1)){
                System.out.println();
                count++;
            }
        }

        //Pick a number of unique cards
        System.out.print("\n\n\n" +
                "*************************************************\n" +
                "===========      Deal random cards      =========\n" +
                "*************************************************\n");
        Scanner input = new Scanner(System.in);
        System.out.print("Pick a number of cards from 1-52: ");
        int pick = input.nextInt();
        while(pick <= 0 || pick > 52){
            System.out.print("Really? Please enter again: ");
            pick = input.nextInt();
        }

        int[] arr = game.Game(pick);
        int i = 0;
        for (Integer pickedDeck : arr) {
            if(i++ == arr.length - 1){
                //Last iteration
                System.out.print(pickedDeck);
            }else{
                System.out.print(pickedDeck+", ");
            }

        }
    }
}
