/**
 * Test
 */
public class Test {

    public static void main(String[] args) {
        short x = 5;
        long y = 15.5;
        //Float z = Float.valueOf(x) + Float.valueOf(y);
        float z = (float)(x+y);
        System.out.println(z); //error: incompatible types: possible lossy conversion from double to long
    }
}