/**
 * Exercise1
 */
public class Exercise1 {

    public static void main(String[] args) {
        Boolean first =true;
        Boolean second =true;

        String a1 = String.valueOf(first);
        String b1 = String.valueOf(second);

        //Boolean cannot be converted to int [error message]
        // int a2 = (int)first;
        // int b2 = (int)second;

        int a2 = (first) ? 1 : 0;
        int b2 = (second) ? 1 : 0;

        System.out.printf("%s and %s",a1, b1);
        System.out.println();

        int addResult = a2 + b2;
        System.out.println(addResult);
    }
}