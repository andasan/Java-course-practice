/**
 * CreateTestArray
 */
import java.util.Arrays; 
public class ProjectA {

    public static void createTestArray(int arraySize)
    {
        int max = 10;
        int min = 1;
        int range = max-min+1;
        int[] anArray = new int[arraySize]; // generate a new int[]
        
        for(int i = 0; i < anArray.length; i++){
            anArray[i] = (int)(Math.random() * range) + min;
        }

        System.out.println(Arrays.toString(anArray));
    }

    public static void main(String[] args) {
        createTestArray(20);
    }
}