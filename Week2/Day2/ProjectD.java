/**
 * ProjectD
 */
import java.util.Arrays; 
public class ProjectD {
    public static void createTestArray(int arraySize)
    {
        int[] anArray = new int[arraySize]; // generate a new int[]

        //insert random numbers inside anArray
        for(int x = 0; x<anArray.length;x++){
            anArray[x] = (int)(Math.random() * 100);
        }

        //Before sorting....
        System.out.println("Before sorting, the values are: " +Arrays.toString(anArray));

        //Insertion Sort 
        for (int i=1; i<anArray.length; ++i) 
        { 
            int key = anArray[i]; 
            int j = i-1; 

            while (j>=0 && anArray[j] > key) 
            { 
                anArray[j+1] = anArray[j]; 
                j = j-1; 
            } 
            anArray[j+1] = key; 
        }

        //After sorting....
        System.out.println("After sorting, the values are: " +Arrays.toString(anArray));
        
    }

    public static void main(String[] args) {
        createTestArray(20);
    }
}