import java.util.Arrays; 

public class ExerciseTwo{
    public static void swap(int[] arr, int i, int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
        //System.out.println("The unchanged list is: " + Arrays.toString(list)); 
    }
    public static void doEx2(){
        int[] list = {1, 2, 3, 4};
        //System.out.println("The unchanged list is: " + Arrays.toString(list)); // A method returns a string representation of the contents of the specified data type array.
        swap(list, 0, 3); //function call of swap
        //System.out.println("The new list is: " + Arrays.toString(list)); 
        System.out.print("The changed list is: ");
        for (int num : list)
            System.out.print(num + " ");
        System.out.println();
    }
}
    