/**
 * ProjectC
 */
import java.util.Arrays;
public class ProjectC {
    public static void createTestArray(int arraySize)
    {
        int[] anArray = new int[arraySize]; // generate a new int[]

        for (int i = 0; i < anArray.length; i++) {
            anArray[i] = (int)(Math.random() * 100); // generate a random number
        }

        System.out.println(Arrays.toString(anArray));
    }

    public static void main(String[] args) {
        createTestArray(1000);
    }
}