/**
 * ExerciseOne
 */
import java.util.Scanner; 
public class ExerciseOne{
    public static void doEx1() { //init static function
        long binary1, binary2; //init long var
        int i = 0, remainder = 0; 
        int[] sum = new int[20]; 

        Scanner in = new Scanner(System.in); //init keyboard input
        System.out.print("Input first binary number: "); 
        binary1 = in.nextLong(); //input first binary num
        System.out.print("Input second binary number: "); 
        binary2 = in.nextLong(); //input second binary num

        while (binary1 != 0 || binary2 != 0) { 
            sum[i++] = (int)((binary1 % 10 + binary2 % 10 + remainder) % 2); 
            remainder = (int)((binary1 % 10 + binary2 % 10 + remainder) / 2); 
            binary1 = binary1 / 10; binary2 = binary2 / 10; 
        } 
        if (remainder != 0) { 
            sum[i++] = remainder; 
        } 
        --i; 
        System.out.print("Sum of two binary numbers: "); 
        while (i >= 0) { 
            System.out.print(sum[i--]); 
        } System.out.print("\n"); 

        //in.close();
            
    }

}