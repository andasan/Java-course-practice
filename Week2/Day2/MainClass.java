/**
 * MainClass
 */
import java.util.Scanner;
public class MainClass {

    public static void main(String[] args) {

        System.out.println("Choose one of the following exercises:");
        System.out.println("Exercise 1: Sum of two binary numbers");
        System.out.println("Exercise 2: Swapping array indexes");
        System.out.println("Exercise 3: Displaying an ArrayList");
        System.out.println("Exercise 4: Methods: toCharArray, isLetter, isDigit and isSpaceChar");
        System.out.println("Exercise 5: Convertion of Decimal to Binary");
        System.out.println("Exercise 6: Convertion of Decimal to Hexadecimal");
        System.out.println("Exercise 7: Convertion of Decimal to Octal");
        System.out.println("Exercise 8: Convertion of Binary to Hexadecimal");
        System.out.println("Exercise 9: Convertion of Binary to Octal");
        System.out.println("Exercise 10: StringBuilder .append() Method");
        System.out.println();
        Scanner input = new Scanner(System.in);
        System.out.print("Choose an exercise to execute (From 1 to 10)");
        int whichExer = input.nextInt();

        while(whichExer != 0){
            switch (whichExer) {
                case 1:
                    ExerciseOne.doEx1(); //Calling function of doEx1 in ExerciseOne Class
                    break;
                case 2:
                    ExerciseTwo.doEx2(); //Calling function of doEx2 in ExerciseTwo Class
                    break;
                case 3:
                    ExerciseThree.doEx3(); //Calling function of doEx3 in ExerciseThree Class
                    break;
                case 4:
                    ExerciseFour.doEx4(); //Calling function of doEx4 in ExerciseFour Class
                    break;
                case 5:
                    ExerciseFive.doEx5(); //Calling function of doEx5 in ExerciseFive Class
                    break;
                case 6:
                    ExerciseSix.doEx6(); //Calling function of doEx6 in ExerciseSix Class
                    break;
                case 7:
                    ExerciseSeven.doEx7(); //Calling function of doEx7 in ExerciseSeven Class
                    break;
                case 8:
                    ExerciseEight.doEx8(); //Calling function of doEx8 in ExerciseEight Class
                    break;
                case 9:
                    ExerciseNine.doEx9(); //Calling function of doEx9 in ExerciseNine Class
                    break;
                case 10:
                    ExerciseTen.doEx10(); //Calling function of doEx10 in ExerciseTen Class
                    break;
            }
            System.out.print("Choose an exercise to execute (From 1 to 10)");
            whichExer = input.nextInt();
        }
        input.close();
    }
}