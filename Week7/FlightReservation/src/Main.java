import java.util.*;

public class Main {

    private static final int FIRST_CLASS_MAX = 1;
    private static final int ECONOMY_CLASS_MAX = 3;
    private static int firstClass = FIRST_CLASS_MAX;
    private static int economyClass = ECONOMY_CLASS_MAX;
    private static ArrayList<String> passengerNames = new ArrayList<>();
    private static ArrayList<ArrayList> databaseManifest = new ArrayList<>();
    private static int flightNo = 1900;

    public static void processManifest(){
        System.out.println("----------PASSENGER LIST OF FLIGHT BG"+flightNo+"----------");
        for(String list: passengerNames) System.out.println(list);
        System.out.println("----------------------------------------------------");
    }

    public static void printItinerary(int seatClass, String firstName, String lastName){
        String classCat;
        if(seatClass == 1) classCat = "First Class";
        else classCat = "Economy Class";

        System.out.println("----------------------------------------------------" +
             "\n---------------------Itinerary----------------------"+
             "\n----------------------------------------------------" +
             "\nPassenger: "+firstName+" "+lastName+
            "\nBooking Ref: PCGYRD"+
            "\n----------------------------------------------------"+
            "\nFrom: KTX"+
            "\nTo: HAN"+
            "\nFlight: BG"+flightNo+
            "\nDeparture: 12:40, Apr 29 2030"+
            "\nArrival: 16:55, Apr 29 2039"+
            "\nClass: "+classCat+
            "\nSeat: 1A"+
            "\nBaggage: 2PC (Max 25KG each)"+
            "\nOperated By: BIGA Air"+
            "\nMarketed By: BIGA Air"+
            "\nBooking Status (1): OK"
        );
    }

    public static void processNewFlight(){
        databaseManifest.add(passengerNames);
        passengerNames.clear();
        flightNo++;
        firstClass = FIRST_CLASS_MAX;
        economyClass = ECONOMY_CLASS_MAX;
        System.out.println("You have chosen to book in another flight :)");
        processHandler(true);
    }

    public static void processClass(int userClass, String firstName, String lastName){

        if(userClass == 1 && firstClass != 0){
            passengerNames.add("BG"+flightNo+": "+firstName+" "+lastName+" [First Class]");
            firstClass--;
            System.out.println("Successfully booked a First Class seat!");
            printItinerary(userClass, firstName, lastName);
            processHandler(true);
        }
        else if(userClass == 2 && economyClass != 0){
            passengerNames.add("BG"+flightNo+": "+firstName+" "+lastName+" [Economy Class]");
            economyClass--;
            printItinerary(userClass, firstName, lastName);
            processHandler(true);
        }
        else if(firstClass == 0 && economyClass == 0){
            System.out.println("The flight is fully booked! Please rebook another flight");
            processHandler(false);
        }
        else if(firstClass >= 0){
            System.out.println("First Class has been fully booked!");
            processHandler(false);
        }
        else if(economyClass >= 0){
            System.out.println("Economy Class has been fully booked!");
            processHandler(false);
        }
        else{
            System.out.println("Invalid entry, please try again.");
            processHandler(true);
        }

    }

    public static void processInput(){
        Scanner inp = new Scanner(System.in);
        int userClass = inp.nextInt();

        if(userClass==4) processManifest();
        else if(userClass==3) processNewFlight();
        else if(userClass==0) System.exit(1);
        else{
            System.out.println("Please enter your First Name: ");
            String firstName = inp.next();
            System.out.println("Please enter your Last Name: ");
            String lastName= inp.next();
            processClass(userClass, firstName, lastName);
        }
        //inp.close();
    }

    public static void processHandler(boolean askClass){

        System.out.println("\n=============== Welcome to BIGA Air ===============" +
                           "\n--------------- Terminal Reservation --------------" +
                           "\n- Flight: BG"+flightNo+" |  Departure: 12:40, Apr 29 2030 -" +
                           "\n---------------- Available Seats: "+ (firstClass+economyClass)+" ---------------"+
                           "\n===================================================");

        if(askClass){
            System.out.print("Enter 1 for First Class or 2 for Economy Class: ");
            processInput();
        }

        if(firstClass > 0 && economyClass == 0){
            System.out.println("Would you like to book a First Class seat instead? Press 2"+
                                "\nYou can choose to book in another flight. Press 3");
            System.out.print("Press 0 to cancel the reservation: ");
            processInput();
        }

        if(economyClass > 0 && firstClass == 0){
            System.out.println("Would you like to book an Economy Class seat instead? Press 2"+
                                "\nYou can choose to book in another flight. Press 3");
            System.out.print("Press 0 to cancel the reservation: ");
            processInput();
        }

        if(firstClass == 0 && economyClass == 0){
            System.out.println("You can choose to book in another flight. Press 3");
            System.out.print("Press 0 to cancel the reservation: ");
            System.out.println("[or 4 to see the manifest]");
            processInput();
        }

    }
    public static void main(String[] args) {
        processHandler(true);
    }
}
