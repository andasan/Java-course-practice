import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int processAI(String AIChoice){
        Random random = new Random();
        int AInumber;

        while(true){
            AInumber = random.nextInt((10 - 1) + 1) + 1;

            if(AIChoice.equals("odd") && AInumber%2==1) break;
            else if(AIChoice.equals("even") && AInumber%2==0) {
                break;
            }else continue;
        }
        return AInumber;
    }

    public static void processGame(String userName, String playerSelection){
        Scanner input = new Scanner(System.in);
        String playerChoice = (playerSelection.equals("O") || playerSelection.equals("o") ) ? "odd" : "even";
        String AIChoice = (playerChoice.equals("odd")) ? "even" : "odd";
        System.out.println(userName+" has picked "+playerChoice+"! Computer will be "+AIChoice);
        System.out.println("===================================================");

        int playerFingers;

        while(true){
            System.out.println("How many finger(s) do you want to show? ");
            playerFingers = input.nextInt();
            if(playerFingers%2==0 && playerChoice.equals("odd")){
                System.out.println("You have chosen "+playerChoice+ "but your finger(s) are EVEN numbers. Please try again.");
            }else if(playerFingers%2==1 && playerChoice.equals("even")){
                System.out.println("You have chosen "+playerChoice+ " but your finger(s) are ODD numbers. Please try again.");
            }else break;
        }
        int AIfingers = processAI(AIChoice);

        System.out.println(userName+" has shown "+playerFingers+" finger(s)! Computer has shown "+AIfingers+" finger(s)!");
        System.out.println("===================================================");

        int sum = playerFingers+AIfingers;

        if((sum%2==0 && playerChoice.equals("even")) || (sum%2==1 && playerChoice.equals("odd"))){
            System.out.println(playerFingers+" + "+AIfingers+ " = "+sum+ " is "+playerChoice);
            System.out.println(userName+" WINS!");
        }
        else{
            System.out.println(playerFingers+" + "+AIfingers+ " = "+sum+ " is "+AIChoice);
            System.out.println("Computer WINS!");
        }

    }

    public static void processHandler(){
        Scanner input = new Scanner(System.in);
        System.out.println("\n===================================================" +
                "\n=============== Game of ODDS or EVEN ===============" +
                "\n===================================================");
        System.out.println("What's your name? ");
        String userName = input.nextLine();

        System.out.print("Hi! "+userName+", would you like to choose (O)dds or (E)vens?");
        String playerSelection = input.nextLine().toLowerCase();

        processGame(userName, playerSelection);

    }
    public static void main(String[] args) {
        processHandler();
    }
}
