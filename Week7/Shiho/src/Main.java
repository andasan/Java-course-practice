import java.util.ArrayList;
import java.util.Scanner;


public class Main {

    static int[][] grid = {
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
    };

    static Scanner scanner = new Scanner(System.in);

    static int x;
    static int y;


    static int userNum = 5;
    static int AINum = 5;


    public static void playground(int[][] grid) {

        // j is x coordinate.
        // top of the playground.
        System.out.print(" ");
        for (int j = 0; j <= 9; j++) {
            System.out.print(" "+j+" ");
        }
        System.out.println();

        // i is y coordinate.
        // left and right of the playground.
        for (int y = 0; y < 10; y++) {
            System.out.print(y);
            for (int x = 0; x <= 9; x++) {
                if( grid[x][y] == 0) {
                    System.out.print(" . ");
                } else if (grid[x][y] == 1) {
                    System.out.print(" @ ");
                } else if ( grid[x][y] == 2) {
                    System.out.print(" ¥ ");
                }
                else if (grid[x][y] == 3) {
                    System.out.print(" ! ");
                }
                else if (grid[x][y]== 4 ) {
                    System.out.println(" x ");
                }
            }
            System.out.println("  " + y);

        }


        // the bottom of the playground.

        System.out.print(" ");
        for (int j = 0; j <= 9; j++) {
            System.out.print(" "+j+" ");
        }
        System.out.println();

    }

    public static void askFirst() {
        System.out.println("Welcome to Battle ship game!!!");
        System.out.println("Here is the playground.");
    }

    public static void userShip() {
        System.out.println("Where do you want to deploy your ships?");
        System.out.println("At first, both you and cp have 5 ships.");

        Main myObj = new Main();
        System.out.println(myObj.userNum);

        for ( int k = myObj.userNum; k > 0 ; k--) {
            System.out.print("tell me x coordinate. : ");
            x = scanner.nextInt();


            System.out.print("tell me y coordinate. : ");
            y = scanner.nextInt();

            grid[x][y] = 1;
        }
    }

    public static void AIShip() {
//		grid[x][y] == 
        for( int i = AINum; i > 0; i--) {
            int xAI = (int)(Math.random() * 9);
            int yAI = (int)(Math.random() * 9);
            grid[xAI][yAI] = 2;
        }
    }

    public static void UserCheckShipSunk() {
        System.out.print("tell me x coordinate. : ");
        int x = scanner.nextInt();

        System.out.print("tell me y coordinate. : ");
        int y = scanner.nextInt();

        if(grid[x][y] == 2) {
//			コンピューターがいた場合、沈める
            grid[x][y] = 3;
            AINum--;
        }

    }

    public static void AICheckShipSunk() {

        for( int i = userNum; i > 0; i--) {
            int xAI = (int)(Math.random() * 9);
            int yAI = (int)(Math.random() * 9);

            if (grid[xAI][yAI] == 1) {
                grid[x][y] = 4;
                userNum--;
            }
            grid[xAI][yAI] = 2;

        }
    }

    public static void main(String[] args) {
        askFirst();
        System.out.println("");
        playground(grid);
        userShip();
        AIShip();

        System.out.println("");
        playground(grid);
//		
//		System.out.println("");
//		UserCheckShipSunk();
//		playground(grid);
//		
//		System.out.println("");
//		AICheckShipSunk();		
//		playground(grid);
    }
}