import java.util.Random;
import java.util.Scanner;

public class Game {
    BattleField bField = new BattleField();
    YourShip[] yourShips = new YourShip[5];
    Enemy[] enemy = new Enemy[5];
    Scanner input = new Scanner(System.in);
    private boolean gameOver = false;
    private boolean AImove = false;
    private int playerLives = 5;
    private int enemyLives = 5;

    public Game(){
        for(int i=0; i< yourShips.length;i++)
            yourShips[i] = new YourShip();

        //display init field
        bField.displayField();

        //Using Array type to contain cards' rank and suit
        for(int x = 0; x < yourShips.length; x++){
            int num = x+1;
            System.out.print("Enter ship# "+ num +"'s X coordinate: ");
            yourShips[x].Xcoord = input.nextInt();
            System.out.print("Enter ship# "+ num +"'s Y coordinate: ");
            yourShips[x].Ycoord = input.nextInt();

            if(yourShips[x].Xcoord > 9 || yourShips[x].Ycoord > 9){
                System.out.println("Please only enter digits from 0 to 9");
                x--;
                continue;
            }

            if(bField.grid[yourShips[x].Xcoord][yourShips[x].Ycoord] == 1){
                System.out.println("A ship exists in this coordinate. Please try again.");
                x--;
            }
            else{
                bField.grid[yourShips[x].Xcoord][yourShips[x].Ycoord] = 1;
            }
        }

        clearConsole();
        bField.displayField();

        try {
            System.out.println("\nComputer is thinking. . . . . . ");
            AIinit(bField);
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

        clearConsole();
        battleStart(bField);
    }

    void AIinit(BattleField bField){
        Random rand = new Random();

        for(int i=0; i< enemy.length;i++) enemy[i] = new Enemy();

        for(int x = 0; x < enemy.length; x++){
            enemy[x].Xcoord = rand.nextInt(10);
            enemy[x].Ycoord = rand.nextInt(10);

            if(bField.grid[enemy[x].Xcoord][enemy[x].Ycoord] == 1 || bField.grid[enemy[x].Xcoord][enemy[x].Ycoord] == 2) x--;
            else bField.grid[enemy[x].Xcoord][enemy[x].Ycoord] = 2;
        }
    }

    void AIturn(BattleField bField){
        Random rand = new Random();
        int x= 0, y = 0;
        while(AImove){
            x = rand.nextInt(10);
            y = rand.nextInt(10);

            if(bField.grid[x][y] == 2 || bField.grid[x][y] == 4 || bField.grid[x][y] == 5){
                continue;
            }
            else if(bField.grid[x][y] == 1){
                System.out.println("\n\nStatus: Player Killed!!");
                bField.grid[x][y] = 4;
                playerLives--;
                AImove = false;
            }
            else if(bField.grid[x][y] == 0){
                System.out.println("Computer Missed!");
                bField.grid[x][y] = 5;
                AImove = false;
            }
        }

        System.out.println("X: "+x+" Y: "+y);
        if(enemyLives == 0 || playerLives == 0) gameOver = true;

        System.out.println("----------------LIVES---------------");
        System.out.println("--------Player: "+playerLives+" | Enemy: "+enemyLives+"--------");
    }

    void battleStart(BattleField bField){
        int x, y;

        System.out.println("GAME START!!!");
        while(!gameOver){
            bField.displayField();
            System.out.print("Enter X coordinate to hit: ");
            x = input.nextInt();
            System.out.print("Enter Y coordinate to hit: ");
            y = input.nextInt();

            if(x>9 || y>9){
                System.out.println("Please only enter digits from 0 to 9");
                continue;
            }

            if(bField.grid[x][y] == 1){
                System.out.println("Do you really want to hit yourself? Try again.......");
                continue;
            }
            else if(bField.grid[x][y] == 2){
                System.out.println("\n\nStatus: Enemy Killed!!");
                bField.grid[x][y] = 3;
                enemyLives--;

            }else if(bField.grid[x][y] == 3){
                System.out.println("Wasted your turn, that enemy has been killed already.");
                bField.grid[x][y] = 3;
            }
            else if(bField.grid[x][y] == 0 || bField.grid[x][y] == 5){
                System.out.println("You Missed!");
                bField.grid[x][y] = 5;
            }

            System.out.println("X: "+x+" Y: "+y);

            if(enemyLives == 0 || playerLives == 0){
                gameOver = true;
                break;
            }

            System.out.println("----------------LIVES---------------");
            System.out.println("--------Player: "+playerLives+" | Enemy: "+enemyLives+"--------");
            bField.displayField();

            try {
                System.out.println("\nComputer's turn. . . . . . ");
                AImove = true;
                Thread.sleep(2000);
                AIturn(bField);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }

        String result = (enemyLives > playerLives) ? "Enemy Won" : "You Won";
        bField.showEnemy = true;
        bField.displayField();
        System.out.println("\n======GAME OVER!!!! "+result+"======");

    }



    public static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");
            if (os.contains("Windows")) Runtime.getRuntime().exec("cls");
            else Runtime.getRuntime().exec("clear");
        }
        catch (final Exception e) {
            //  Handle any exceptions.
        }
    }
}
