public class BattleField {

    private final int X = 10;
    private final int Y = 10;
    boolean showEnemy = false;
    int[][] grid = {
                    {0,0,0,3,0,0,0,0,0,0,},
                    {0,0,0,0,0,0,0,0,0,0,},
                    {0,0,0,4,0,0,5,0,0,0,},
                    {0,0,0,0,0,0,0,0,0,0,},
                    {0,0,0,0,0,0,0,0,0,0,},
                    {0,0,0,0,0,0,0,0,0,0,},
                    {0,0,0,0,0,0,0,0,0,0,},
                    {0,0,0,0,0,0,0,0,0,0,},
                    {0,0,0,0,0,0,0,0,0,0,},
                    {0,0,0,0,0,0,0,0,0,0,}
                    };
    YourShip yourShip = new YourShip();
    Enemy enemy = new Enemy();

    public void displayField(){
        // Displaying the grid
        //top coords
        System.out.print("   ");
        for(int j = 0; j <= 9; j++){
            System.out.print(" "+j+" ");
        }
        System.out.println();

        for (int i = 0; i < X; i++)
        {
            System.out.print(" "+i+" "); //left coords
            for (int j = 0; j < Y; j++)
            {
                if (grid[i][j] == 0) //blank
                    System.out.print(" . ");
                else if(grid[i][j] == 1) //player
                    System.out.print(yourShip.YELLOW+" "+yourShip.mySymbol+" "+yourShip.RESET);
                else if(grid[i][j] == 2){ //enemy
                    if(showEnemy) System.out.print(enemy.PURPLE+" "+enemy.mySymbol+" "+enemy.RESET);
                    else System.out.print(" . ");
                }
                else if(grid[i][j] == 3) //enemy killed status
                    System.out.print(enemy.RED+" "+enemy.mySymbol+" "+enemy.RESET);
                else if(grid[i][j] == 4) //player killed status
                    System.out.print(yourShip.RED+" "+yourShip.mySymbol+" "+yourShip.RESET);
                else if(grid[i][j] == 5) //missed
                    System.out.print("\u001B[31m X \033[0m");
            }
            System.out.println("  "+i+"  "); //right coords
        }

        //bottom coords
        System.out.print("   ");
        for(int j = 0; j <= 9; j++){
            System.out.print(" "+j+" ");
        }
        System.out.println();
    }
}
