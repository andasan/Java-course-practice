import java.io.FileNotFoundException;
import java.io.*;
import java.util.*;

public class Main {

    private static File listSource = new File("src/to_do_list.txt");
    public static ArrayList<String> todoList = new ArrayList<>();
    static char checkBox = '\u2610';
    static char tick = '\u2713';

    public static void printMessge(String msg){
        System.out.println("*******************************" +
                            "\n"+msg+
                            "\n*******************************");
    }

    public static void saveToFile(){
        getList(false, false);
        try {
            PrintWriter output = new PrintWriter(listSource);
            for(int x=0; x<todoList.size();x++) {
                output.println(todoList.get(x));
            }
            output.close();
            printMessge("LIST SAVED");
        }
        catch (FileNotFoundException e) {
            System.out.println("File does not exist on the File System");
        }
        promptAfter();
    }

    public static void markAnEntry(){
        Scanner userInput = new Scanner(System.in);

        getList(false, true);
        System.out.print("Choose an entry to mark (type 0 when finished): ");

        while(true){
            int entryMark = userInput.nextInt();

            if((entryMark > 0 && entryMark <= todoList.size()) && todoList.get(entryMark-1).contains(""+tick)){
                printMessge("Entry has already been marked");
                System.out.print("Choose an entry to mark (type 0 when finished): ");
                continue;
            }

            if(entryMark > 0 && entryMark <= todoList.size()) {
                String temp = todoList.get(entryMark - 1);
                todoList.set(entryMark - 1, tick+" "+temp.substring(2,temp.length()-1));
                getList(false, true);
                printMessge("Entry #" + entryMark + " has been marked");
                System.out.print("Choose an entry to mark (type 0 when finished): ");
            }else if(entryMark == 0){
                break;
            } else{
                printMessge("Invalid entry number. Try again");
                System.out.print("Choose an entry to mark (type 0 when finished): ");
                continue;
            }
        }
        promptAfter();
    }

    public static void removeEntry(){
        getList(false, false);
        System.out.println("Select an entry to remove: ");
        Scanner userInput = new Scanner(System.in);
        int entryNumber = userInput.nextInt();

        todoList.remove(entryNumber-1);

        printMessge("Entry #"+entryNumber+" has been removed");
        promptAfter();
    }

    public static void newEntry(){
        Scanner userInput = new Scanner(System.in);
        System.out.print("Enter a new task for your To Do List (type -exit when finished): ");

        while(true){
            String newLineEntry = userInput.nextLine();
            if(!newLineEntry.equals("-exit")) todoList.add(checkBox+" "+newLineEntry);
            else break;
        }

        printMessge("New entry added");
        promptAfter();
    }

    public static void getList(boolean showPrompt, boolean forMarker){
        int count = 1;
        if(!todoList.isEmpty()){
            printMessge("----------THIS IS A LIST-----------");
            for(String showList: todoList){
                if(forMarker){
                    System.out.println(count+") "+showList);
                    count++;
                }else System.out.println(showList);
            }
        }
        else printMessge("List is EMPTY");

        if(showPrompt) promptAfter();
    }

    public static void openFile(){
        try {
            Scanner input = new Scanner(listSource);
            while (input.hasNextLine()){
                todoList.add(input.nextLine());
            }
            input.close();
            getList(true, false);

        } catch (FileNotFoundException e) {
            System.out.println("File does not exist on the File System");
        }

    }

    public static void promptAfter(){
        Scanner userInput = new Scanner(System.in);
        System.out.print("RETURN TO MENU? [Y/N]: ");
        while(true){
            String returnToMenu = userInput.next().toLowerCase();

            if(returnToMenu.equals("y")){
                showMenu();
                break;
            }
            else if(returnToMenu.equals("n")){
                exitProgram();
                break;
            }
            else{
                System.out.print("Invalid Entry. Try again: ");
            }
        }

    }

    public static void exitProgram(){
        System.exit(1);
    }

    public static void showMenu(){
        Scanner userInput = new Scanner(System.in);
        System.out.println("--------- TO DO LIST ----------\n" +
                "Choose from one of the commands:\n" +
                "0 - Exit the program.\n" +
                "1 - Show List items\n" +
                "2 - New entry\n" +
                "3 - Remove an entry\n" +
                "4 - Mark a list\n" +
                "5 - Save\n" +
                "6 - Load");

        System.out.print("--------------------------------\nSelect a command:");
        int initCommand = userInput.nextInt();
        //userInput.close();

        switch (initCommand){
            case 0:
                exitProgram();
                break;
            case 1:
                getList(true, false);
                break;
            case 2:
                newEntry();
                break;
            case 3:
                removeEntry();
                break;
            case 4:
                markAnEntry();
                break;
            case 5:
                saveToFile();
                break;
            case 6:
                openFile();
                break;
        }
    }

    public static void main(String[] args) {
        showMenu();
    }
}
