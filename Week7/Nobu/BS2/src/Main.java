import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Screen myScreen = new Screen();
        Ship[] ship = new Ship[2];
        Scanner input = new Scanner(System.in);


        System.out.println("Game start!!");
        myScreen.drawScreen();


        // Deploy ships
        for(int i = 0; i <2;i++) {
            ship[i] = new Ship();
            System.out.println("Deploy ship" + i + " position x: ");

            ship[i].x = input.nextInt();

            System.out.println("Deploy ship" + i + " position y: ");

            ship[i].y = input.nextInt();

        }


        myScreen.update(ship);

    }

}
