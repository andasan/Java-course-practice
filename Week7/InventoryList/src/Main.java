import java.util.*;

public class Main {
    static double[] priceList = {4.90,5.80,15.50,9.90,11.50};
    static ArrayList<Integer> productName = new ArrayList<>();
    static ArrayList<Integer> productQuantity = new ArrayList<>();
    static int userSelection;
    static int quantity;
    static boolean flagged;

    public static void printReceipt(){
        int totalQuantity=0;
        double sum;
        double total = 0.0;
        System.out.println("Here's your receipt!");
        System.out.println("========================================" );

        System.out.println("Product Name    Quantity    Total Price" +
                           "\n----------------------------------------");
        for(int i = 0; i< productName.size(); i++){
            sum = productQuantity.get(i) * priceList[productName.get(i)-1];
            total+=sum;
            totalQuantity+= productQuantity.get(i);
            System.out.printf("\nProduct-%d    %7d    %15.2f", productName.get(i), productQuantity.get(i), sum);
        }
        System.out.printf("\n\n----------------------------------------" +
                          "\n             %7d    %15.2f", totalQuantity, total);
    }

    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);
        while(true){
            flagged = false;
            System.out.println("Please select a product (1-5) or (-1) to print receipt. Enter (0) to exit"+
                               "\n===========================================================================" );
            userSelection = inp.nextInt();

            if(userSelection>5 || userSelection<-1){
                System.out.println("Please only enter a product from 1-5");
                continue;
            }
            else if(userSelection == -1) break;
            else if(userSelection == 0) System.exit(1);

            System.out.println("Please enter a quantity for Product-"+userSelection+
                               "\n=======================================================================" );
            quantity = inp.nextInt();

            for(int i = 0; i< productName.size(); i++){
                if(userSelection == productName.get(i)){
                    flagged = true;
                    int temp = productQuantity.get(i) + quantity;
                    productQuantity.set(i,temp);
                    continue;
                }
            }

            if(!flagged){
                productName.add(userSelection);
                productQuantity.add(quantity);
            }

        }
        printReceipt();
    }
}
