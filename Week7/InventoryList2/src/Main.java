import java.util.*;

public class Main {
    static String[][] productArray = new String[5][2];
    static ArrayList<String> productName = new ArrayList<>();
    static ArrayList<Integer> productQuantity = new ArrayList<>();
    static int userSelection;
    static int quantity;
    static boolean flagged;

    private static void initProduct(){
        productArray[0][0] = "Product A";
        productArray[0][1] = "4.90";
        productArray[1][0] = "Product B";
        productArray[1][1] = "5.80";
        productArray[2][0] = "Product C";
        productArray[2][1] = "15.50";
        productArray[3][0] = "Product D";
        productArray[3][1] = "9.90";
        productArray[4][0] = "Product E";
        productArray[4][1] = "11.50";
    }

    private static int indexOf(String[][] arr, int row, String x) {
        for(int i=0; i<row; i++) {
            if(arr[i][0].equals(x)) return i;
        }
        return -1;
    }

    public static void printReceipt(){
        int x, totalQuantity=0;
        double d, sum;
        double total = 0.0;
        System.out.println("Here's your receipt!");
        System.out.println("========================================" );

        System.out.println("Product Name    Quantity    Total Price" +
                "\n----------------------------------------");
        for(int i = 0; i< productName.size(); i++){
            x = indexOf(productArray, productArray.length, productName.get(i));
            //x = Arrays.asList(productArray).indexOf(productName.get(i));
            d = Double.parseDouble(productArray[x][1]);
            sum = productQuantity.get(i) * d;
            total+=sum;
            totalQuantity+= productQuantity.get(i);
            System.out.printf("\n%-19s%-14d$%.2f", productName.get(i), productQuantity.get(i), sum);
        }
        System.out.printf("\n\n----------------------------------------" +
                "\n%-19s%-14d$%.2f", "", totalQuantity, total);
    }

    public static void main(String[] args) {
        initProduct();
        Scanner inp = new Scanner(System.in);
        while(true){
            flagged = false;
            System.out.println("Please select a product (1-5) or (-1) to print receipt. Enter (0) to exit"+
                    "\n===========================================================================" );
            userSelection = inp.nextInt();

            if(userSelection>5 || userSelection<-1){
                System.out.println("Please only enter a product from 1-5");
                continue;
            }
            else if(userSelection == -1) break;
            else if(userSelection == 0) System.exit(1);

            System.out.println("Please enter a quantity for "+productArray[userSelection-1][0] );
            quantity = inp.nextInt();

            for(int i = 0; i< productName.size(); i++){
                if(productArray[userSelection-1][0].equals(productName.get(i))){
                    flagged = true;
                    int temp = productQuantity.get(i) + quantity;
                    productQuantity.set(i,temp);
                    continue;
                }
            }

            if(!flagged){
                productName.add(productArray[userSelection-1][0]);
                productQuantity.add(quantity);
            }

        }
        printReceipt();
    }
}
//
//public class Main{
//public static void main(String[] arg) {
//
//        Scanner in = new Scanner(System.in);
//
//        int [][] matrix = { // 5 rows, 7 cols
//        { 9, 13, 4, 7, 1, 14, 10},
//        { 8, 2, 12, 11, 6, 15, 2},
//        { 9, 6, 7, 10, 15, 8, 3},
//        { 12, 14, 8, 15 ,2 , 7, 8},
//        { 12, 10, 3, 11, 8, 3, 5},
//        };
//
//        String [] product = new String [5];
//        product [0] = "MP3 Player";
//        product [1] = "Smart Phone";
//        product [2] = "Digital Watch";
//        product [3] = "Tablet";
//        product [4] ="Portable Gaming System";
//
//        double [] price = {10.75, 15.27, 5.98, 9.67, 4.32, 12.50, 1.42}; // 7 elements
//
//        costOfEach(matrix, product, price);
//        }
//
//    // compute and display the cost of manufacturing each device
//    public static double costOfEach(int matrix[][], String[] product, double price [] ){
//        double cost = 0.00;
//        String item = "";
//        double maxCost = 0.00;
//        double minCost = Double.MAX_VALUE;
//        int maxCostIndex = 0;
//        int minCostIndex = 0;
//
//        String format = "%-40s$%.2f%n";
//        for (int row = 0; row < matrix.length; row++){
//        cost = 0;
//        for (int col = 0, i = 0; col < matrix[0].length && i < price.length; col++, i++){
//
//        cost += matrix[row][col] * price [i];
//        }
//
//        System.out.printf(format, product[row] + " cost:", cost);
//        }
//        return cost;
//    }
//}
