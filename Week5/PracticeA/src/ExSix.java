import java.util.Scanner;

public class ExSix {
    public static void ExSix(){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a string");
        String someString = input.next();
        System.out.println("Enter a character to find");
        String findThis = input.next();

        if(someString.indexOf(findThis) >= 0) System.out.println("HIT");
        else System.out.println("MISS");

    }
}
