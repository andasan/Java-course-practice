import java.util.Scanner;

public class ExNine {
    public static void ExNine(){
        int vowel = 0;
        int consonant = 0;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a string");
        String someString = input.next();
        for(int i=0; i<someString.length(); i++) {

            if(someString.charAt(i) == 'a'|| someString.charAt(i) == 'e'|| someString.charAt(i) == 'i' ||
                    someString.charAt(i) == 'o' || someString.charAt(i) == 'u') {
                vowel++;
            } else {
                consonant++;
            }
        }
        System.out.println("Number of consonant(s): "+consonant+" and vowel(s): "+vowel);

    }
}
