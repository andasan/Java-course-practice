import java.util.Scanner;

public class ExEight {
    public static void ExEight(){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a string");
        String someString = input.next();
        boolean isItPalindrome = true;
        char[] ch = someString.toCharArray();
        int i1 = 0;
        int i2 = ch.length - 1;
        while (i2 > i1) {
            if (ch[i1] != ch[i2]) {
                isItPalindrome = false;
            }
            i1++;
            i2--;
        }
        System.out.println("String: "+someString+" is a Palindrome? "+isItPalindrome);
    }
}
