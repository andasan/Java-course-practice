import java.util.Scanner;

public class ExSeven {
    public static void ExSeven(){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a string");
        String someString = input.next();
        System.out.println("Enter a repeated character to count");
        String findThis = input.next();
        int count = 0;

        char[] ch = someString.toCharArray();
        for(int i = 0; i < someString.length(); i++){
            if(findThis.indexOf(ch[i]) >= 0 ){
                count++;
            }
        }
        System.out.println("Number of repetitions for the char "+findThis+" is: "+count);
    }
}
