import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Choose an exercise to execute (From 1 to 10)");
        int whichExer = input.nextInt();

        while(whichExer != 0){
            switch (whichExer) {
                case 1:
                    ExOne.ExOne(); //Calling function of doEx1 in ExerciseOne Class
                    break;
                case 2:
                    ExTwo.ExTwo(); //Calling function of doEx2 in ExerciseTwo Class
                    break;
                case 3:
                    ExThree.ExThree(); //Calling function of doEx3 in ExerciseThree Class
                    break;
                case 4:
                    ExFour.ExFour(); //Calling function of doEx4 in ExerciseFour Class
                    break;
                case 5:
                    ExFive.ExFive(); //Calling function of doEx5 in ExerciseFive Class
                    break;
                case 6:
                    ExSix.ExSix(); //Calling function of doEx6 in ExerciseSix Class
                    break;
                case 7:
                    ExSeven.ExSeven(); //Calling function of doEx7 in ExerciseSeven Class
                    break;
                case 8:
                    ExEight.ExEight(); //Calling function of doEx8 in ExerciseEight Class
                    break;
                case 9:
                    ExNine.ExNine(); //Calling function of doEx9 in ExerciseNine Class
                    break;
                case 10:
                    ExTen.ExTen(); //Calling function of doEx10 in ExerciseTen Class
                    break;
            }
            System.out.print("Choose an exercise to execute (From 1 to 10)");
            whichExer = input.nextInt();
        }
        input.close();
    }

}
