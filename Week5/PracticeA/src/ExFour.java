import java.util.Scanner;

public class ExFour {
    public static void ExFour(){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a letter:");
        String letter = input.next();
        switch(letter){
            case "a":
                System.out.println("Vowel");
                break;
            case "e":
                System.out.println("Vowel");
                break;
            case "i":
                System.out.println("Vowel");
                break;
            case "o":
                System.out.println("Vowel");
                break;
            case "u":
                System.out.println("Vowel");
                break;
            default:
                System.out.println("Consonant");
                break;
        }

    }
}
