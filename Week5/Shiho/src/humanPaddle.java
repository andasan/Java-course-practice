import java.awt.Color;
import java.awt.Graphics;

public class humanPaddle implements Paddle{
	
	
	// x, y represent paddle position.
	// in this case, vel means how much it move.
	// the reason why y is double is y is greater than x.
	static double y, yVel;
	
	// moving up and down faster.. I don't get it. but the guy in video said.
	boolean upAccel, downAccel;
	
	final double GRAVITY = 0.94;
	
	// player means both left and right paddle??
	int player, x;
	
	int score;
	
	
	// contructor
	public humanPaddle(int player) {
		
		// initialize at first
		upAccel = false;
		downAccel = false;
		y = 210;
		
		// not moving
		yVel = 0;
		
		// player == 1 means first player.
		// so player 1's paddle is located in x = 20
		if( player == 1) {
			x = 20;
		} else {
			
			//player 2's paddle is located in x = 660
			 x = 660; 
		}
		
		score = 0;
	}
	
	// it doesn't work because of it! 
	// probably the error caused by parameter
	public void draw(Graphics g) {
		g.setColor(Color.pink);
		
		// fillRect make the ractangle.
		// the reason why (int) y is y was ordinally double.
		g.fillRect(x, (int)y, 20, 80);
		
		
	}

	// move method indicate how the paddle move.
	public void move() {
		
		// if it was ordered " go up "
		if (upAccel) {
			
			// why - ? 
			// because go up means value of y decrease.
			yVel -= 5;
			
		// if it was orderd "go down"
		
		} else if ( downAccel ) {
			
			// why + ? 
			// because go down means value of y increase.
			yVel += 2;
		
		// if it was ordered neither them.	
		// probably if no button was pressed, the paddle goes down by gravity
		// in order not to stop it suddenly and to stop it slowly.
		} else if ( !upAccel && !downAccel ){
			yVel *= GRAVITY;

					
		}
		
		// it prevent the speed to go down faster than 5.
		if ( yVel >= 5) {
			yVel = 5;
			
		// it prevent the speed to go up  faster than 5
		} else if ( yVel <= -5) {
			yVel = -5;
		}
		
		
		// why is it necessary?
		//
		y += yVel;
	

		
		// it prevent the paddle to disappear because the paddle goes up too high 
		if( y < 0) {
			y = 0;
		
//		 it prevent the paddle to disappear because the paddle goes down too down.
//		 when the bottom of paddle reach the bottom of window,
//		y is height of window minus height of paddle. 
//		 in other words, 500 - 80 = 420
		} else if ( y > 420) {
			y = 420;
		}
		}
		

	
	//they are setter class
	// I don't grab what input work.
	
	// this can order the paddle goes up
	public void setUpAccel(boolean input) {
		upAccel = input;
	}
	
	// this can order the paddle goes down.
	public void setDownAccel(boolean input) {
		downAccel = input;
	}
	

	// it is a getter class of y

	public static int getY() {
		return (int)y;
	}

	  public int getScore(){
		  
		  return this.score;
		  
	  }
}
