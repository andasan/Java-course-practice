import java.awt.Color;
import java.awt.Graphics;

public class AIpanel implements Paddle{
	
	
	
	// x, y represent paddle position.
	// in this case, vel means how much it move.
	// the reason why y is double is y is greater than x.
	static double y, yVel;
	
	// moving up and down faster..
//	I don't get it. but the guy in video said.
	boolean upAccel, downAccel;
	
	final double GRAVITY = 0.94;
	
	// player means both left and right paddle??
	int player, x;
	
	// in order to track the ball
	ball b1;
	
	int score;
	
	// contructor
	public AIpanel(int player, ball b) {
		
		// initialize at first
		upAccel = false;
		downAccel = false;
		y = 210;
		
		// not moving
		yVel = 0;
		
		score = 0;
		
		// player == 1 means first player.
		// so player 1's paddle is located in x = 20
		if( player == 1) {
			
			x = 20;
			
		} else {
			
			//player 2's paddle is located in x = 660
			 x = 660; 
		}
		
		
	}
	
	// it doesn't work because of it! 
	// probably the error caused by parameter
	public void draw(Graphics g) {
		g.setColor(Color.CYAN);
		
		// fillRect make the ractangle.
		// the reason why (int) y is y was ordinally double.
		g.fillRect(x, (int)y, 20, 80);
	}

	// move method indicate how the paddle move.
	public void move() {
		
		// I don't grab what he said why 40?
		// in order to hit the ball in the middle of AIpaddle
		// change here otherewise no one can win.
		y = ball.getY() - 40;
		
		// it prevent the paddle to disappear because the paddle goes up too high 
		if( y < 0) {
			y = 0;
		
		// it prevent the paddle to disapear because the paddle goes down too down.
		// when the bottom of paddle reach the bottom of window,
		//y is height of window minus height of paddle. 
		// in other words, 500 - 80 = 420
		} else if ( y > 420) {
			y = 420;
		}
		
	}

	
	

	// it is a getter class of y

	public static int getY() {
		return (int)y;
	}
	
	 public int getScore(){
		 
		 return this.score;
		  
	 }

}

