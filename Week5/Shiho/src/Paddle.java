import java.awt.Graphics;


// in interface, it has some methods in which they don't have anything. 
// in the video, the guy didn't mention static, but my code show error!!
// my code said you need static.

public interface Paddle { 
	
	public static void draw(Graphics g) {
		
	}
	
	public static void move() {
		
	}
	
	// probably this is getter class
	// implement shouldn't have anything inside of method, but it shows an error!!!
	public static int getY() {
		return 0;
	}
	
	
}
