import java.awt.Color;
import java.awt.Graphics;

public class ball {
	static double  xVel, yVel, x, y;
	
	// constructor
	public ball() {
		
		// indicate where the ball is at first
		x = 350;
		y = 250;
		
		// speed of ball
		xVel = getRadomSpeed() * getRandomDirection();
		yVel = getRadomSpeed() * getRandomDirection();
	}
	
	// the speed change from 2 to 5??
	public double getRadomSpeed() {
		return (Math.random() * 3 + 2);
	}
	
	// get random direction of ball 
	public int getRandomDirection() {
		int rand = (int)(Math.random() * 2);
		if (rand == 1) {
			return 1;
		} else {
			return -1;
		}
	}
	
	// show the ball in window because of this
	public void draw(Graphics g) {
		g.setColor(Color.green);
		
		// fillOval make the oval (or circle) and determine location and its width and height.
		// why substract 10?
		// because x and y means top-left
		// x - 10 and y - 10 means the center of ball. 
		
		g.fillOval((int)x - 10, (int)y - 10, 20, 20);
	}
	
	public void checkPaddleCollision(humanPaddle p1, AIpanel p2) {
//		System.out.println("check the collision");
//		System.out.println("            ");
		// 50 is the gap between the left side of window and the right paddle(20), how thick the paddles(20)
		// and the distance the left edge of ball and center of ball (10)
		if ( x-10 <= p1.x + 20) {
			
			//getY method doesn't work
			// 80 means height of panel
			if( y >= p1.getY() && y < p1.getY() + 80) {
				xVel = xVel * -1;
				System.out.println("hit 1");
			}
			
			
			
			// 650 is the gap
			//from the right side( x = 700 ) of window to the right side of the left paddle (x = 680)
			//, how thick the paddle and the distance between the left edge of ball and the center of ball. 
		} 
		
		if ( x + 10 >= p2.x) {
			if( y >= AIpanel.getY() && y < AIpanel.getY() + 80) {
				xVel = xVel * -1;
				System.out.println("hit 2");
				System.out.println("          ");
			}
		}
		
		
	}
	// how the ball move
	public void move() {
		x += xVel;
		y += yVel;
		
		// it makes the ball bounce.
		// When y is smaller 10, it means the ball reached top of window. 
		if ( y < 10 ) {
			yVel = -yVel;
		// when y is greater than 490, it means teh ball reached the bottom of window.
		} else if ( y > 490) {
			yVel = -yVel;
			
		}
		
		if ( x < 0 ) {
			xVel = xVel * -1;
		} else if ( x > 700) {
			xVel = xVel * -1;
		}
	}
	
	//getter method of x and y 
	public static int getX() {
		return (int) x;
	}
	
	public static int getY() {
		return (int) y;
	}
}
