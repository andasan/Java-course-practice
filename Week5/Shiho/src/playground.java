import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.Console;

// implement which means to execute interface class. 
public class playground extends Applet implements Runnable, KeyListener{
	// final try not to be accessed from other class or method?
	final int WIDTH= 700, HEIGHT = 500;
	
	// thread means the sequence of execution?
	static Thread thread;
	
	static Graphics gfx;
	static Image img;
	
	static humanPaddle p1;
	static AIpanel p2;
	
	static ball ball;
	
	
	static String ScoreP1;
	static String ScoreP2;
	
	static boolean gameStarted;
	
	public void init () {
		
		// this is needed to use METHOD outside of this method
		// what does "this" indicate??
		this.resize(WIDTH, HEIGHT);
		
		gameStarted =false;
		
		this.addKeyListener(this);
		
		// parameter was player,
		// now we can know what player indicate.
		// 1 is left, 2 is right.
		p1 = new humanPaddle(1);
	
		ball = new ball();
		
		p2 = new AIpanel(2, ball);
		
		// if they are used, the sentence is not blinking anymore. 
		// the window is drawn every time it is updated.
		// if off-screen is used, that doesn't happen.
		
		// Creates an off-screen drawable image to be used for double buffering
		img = createImage(WIDTH, HEIGHT);
		// used getGraphics method in createImage class
		// Creates a graphics context for drawing to an off-screen image.
		gfx =img.getGraphics();
		
		// start program 
		Thread thread = new Thread(this);
		thread.start();
		
		ScoreP1 = String.valueOf(p1.score); 
		ScoreP2 = String.valueOf(p2.score); 
		 
		
	}
	
	public void paint(Graphics gfx) {
		
		// "this" is image of observer
		// ImageObserver is an interface 
		// that has methods for handling notification of state of image loading
		gfx.drawImage(img, 0, 0, this);
		gfx.setColor(Color.black);
		gfx.fillRect(0, 0, WIDTH, HEIGHT);
		
		
		// how to show score.
		gfx.setColor(Color.cyan);
		gfx.drawString(ScoreP2, 650, 50);
		
		gfx.setColor(Color.pink);
		gfx.drawString(ScoreP1, 30, 50);	
		
		p1.draw(gfx);
		ball.draw(gfx);
		p2.draw(gfx);
	
		// how to show the game over
			//gfx.setColor(Color.red);
//			gfx.drawString("Game over", 350, 250);
//			p1.draw(gfx);
//			ball.draw(gfx);
//			p2.draw(gfx);
		
		if (!gameStarted) {
			gfx.setColor(Color.green);
			gfx.drawString("Pong", 320, 100);
			gfx.drawString("Press Enter to begin.", 280, 130);
		}
		
		
		
		
//		// "this" is image of observer
//		// ImageObserver is an interface 
//		// that has methods for handling notification of state of image loading
//		gfx.drawImage(img, 0, 0, this);
		
		}
		
	
	
	public void update(Graphics gfx) {
		// when update, this code repaint background black in the window.
		paint(gfx);

	}


	// it shows how the this game is drawn in window.
	public void run() {

		for(;;){
			//System.out.println("for");
			System.out.println("ball x: " + ball.getY());
			// gameStarted is true, but this if statement doesn't work and yyy isn't shown.
			if(gameStarted) {
			//System.out.println("inside");
			ball.move();
			p1.move();
			p2.move();
			ball.checkPaddleCollision(p1, p2);
			
			
			// might update how window is drawn many times.
			repaint();
			
			if (ball.getX() == 0 || ball.getX() == 700) {
				System.out.println("reach it");
				System.out.println("                    ");
				if(ball.getY() < p1.y && ball.getY() > p1.y + 80) {
					p2.score += 1;
					System.out.println(p2.score);
				} 
				else if(ball.getY() < p2.y && ball.getY() > p2.y + 80) {
					p1.score += 1;	
					
				}
			}
//			
//			if (ball.getX() == 0){
//				if(ball.getY() < p2.y && ball.getY() > p2.y + 80) {
//					p1.score += 1;	
//				}
//			}
			
			// what does it do?? I'm not sure.	
			try {
				
				// sleep ( which means stop execution?) 10 milliseconds
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// this method is for error.
				e.printStackTrace();
			}
			}
		}
		}
		


	@Override
	public void keyTyped(KeyEvent e) {
		//p1.setUpAccel(true);
	}

	
	// when a button in keyboad is pressed, something happen.
	public void keyPressed(KeyEvent e) {
		
		// Returns an extended key code for the event.
				// VK means "visual Keyboad"
				// KeyEvent.VK_UP means to press up button
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					p1.setUpAccel(true);
					
				// KeyEvent.VK_DOWN means to press down button
				} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					p1.setDownAccel(true);
					
				
				} else if ( e.getKeyCode() == KeyEvent.VK_SPACE ) {
					gameStarted = true;
					//System.out.println(gameStarted);
					
				}
	}


	
	// this method declare what it will do if we release the button (which means not to press button ).
	
	public void keyReleased(KeyEvent e) {
	
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			
			// it means not to go up when the up button is released.
			p1.setUpAccel(false);
			
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			
			// it means not to go down when the down button is released.
			p1.setDownAccel(false);
			
		}
	}
}
