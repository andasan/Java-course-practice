import java.util.concurrent.TimeUnit ;
public class CalcPrimeNum {

    public void doCalculation(int n){
        long startTime = System.nanoTime();

        boolean prime[] = new boolean[n+1]; 

        //create list, make all numbers a prime number (apply true)
        for(int i=0;i<=n;i++) 
            prime[i] = true; 
        
        //by Sieve of Eratosthenes
        //loop through the list and mark the numbers that are multiples of their own
        //run a loop from 2 to the square root of n
        for(int x = 2; x*x <=n; x++) { 
            if(prime[x] == true) { 
                //cancelling out the multiples of x
                for(int i = x*x; i <= n; i += x) 
                    prime[i] = false; 
            } 
        } 

        for(int i = 2; i <= n; i++) { 
            if(prime[i] == true)
                System.out.println(i + " "); 
        }

        long nanoSeconds = System.nanoTime() - startTime;

        double totalSeconds = nanoSeconds/1000000000.0;
        double currentSecond = totalSeconds%60;
        
        System.out.println("Total execution time to create "+n+
                        " objects in milli: " +currentSecond+"seconds. MS= "+nanoSeconds);
    }

    public static boolean isInt(String[] str) {
        try {
            Integer.parseInt(str[0]);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    private static boolean hasParamaters(String[] args){
        return (args.length > 0);
    }

    public static void main(String[] args) {
        if (hasParamaters (args) == true){
            if(isInt (args) == true){
                CalcPrimeNum calc = new CalcPrimeNum();
                calc.doCalculation(Integer.parseInt(args[0]));
            }
            else{
                System.out.println("You need to enter an integer");
            }
        }
        else{
            System.out.println("You didn't enter anything");
        }
    }
}