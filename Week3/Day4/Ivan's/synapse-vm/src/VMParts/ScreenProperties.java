package VMParts;

// Will hold all properties that have to do with the physical layout
// or other variable characteristics of our screen.
// I decided to make this just as a handy way to store some information
// related to the screen. I use it in DisplayController
public class ScreenProperties
	{
	// this represents the width (in chars) for current screen
	public int width = 0;
	// this represents the height (in chars) for current screen
	public int height = 0;
	public void setSize(int width, int height)
		{
		this.width = width;
		this.height = height;
		}
	}