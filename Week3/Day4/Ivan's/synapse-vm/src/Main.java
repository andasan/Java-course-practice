import VMParts.VirtualMachine;


public class Main 
	{
	public static boolean terminateProgram = false;

	public static VirtualMachine vm;

	public static void main(String[] args)
		{
		// here we use this to indicate if our program
		// should shut down or not, the do...while
		// checks for this over and over
		terminateProgram = false;
		
		// Here we use a 'factory pattern' in order
		// to create our class. 
		// See: https://www.tutorialspoint.com/design_pattern/factory_pattern.htm
		vm = VirtualMachine.CreateVirtualMachine();
		// The factory pattern are the 'static' methods inside
		// the VirtualMachine class. It is a programmer trick that means we make less mistakes
		// you will learn many patterns over the years. 
						
		// Begin our do loop, we want to execute the code
		// at least once, so we use do first. and use while
		// afterward. 
		do  {
			// check if our vm is shutting down
			if (vm.isShuttingDown == false)
				{
				System.out.println("Processing");
				// if it isn't, tell our VM to process
				// each item in order
				vm.display.processController();
				vm.keyboard.processController();
				vm.mouse.processController();
				vm.disk.processController();
				}
			else 
				{
				System.out.println("Shutting Down");
				// our vm wants to shut down
				// so we indicate to each module that this
				// is about to happen, afterward we then
				// change the boolean so our application ends
				vm.display.doShutdownProcess();
				vm.keyboard.doShutdownProcess();
				vm.mouse.doShutdownProcess();
				vm.disk.doShutdownProcess();				
				terminateProgram = true;
				}
		
			
			}
		while (terminateProgram == false);
		// if our program code goes past this line, program
		// will end
		}
	
	}



