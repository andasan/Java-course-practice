package VMParts;
	
	
public class VirtualMachine
	{
	public static VirtualMachine CreateVirtualMachine()
		{
		VirtualMachine aVM = new VirtualMachine();
		VirtualMachine.PowerOn(aVM);
		return aVM;
		}
	
	public static void DestroyVirtualMachine(VirtualMachine aVM)
		{
		PowerOff(aVM);
		}
	
	public static void PowerOn(VirtualMachine aVm)
		{
		aVm.display = new DisplayController(800,600);
		aVm.keyboard = new KeyboardController();
		aVm.mouse = new MouseController();
		aVm.disk = new DiskController();
		}

	public static void PowerOff(VirtualMachine aVm)
		{
		aVm.isShuttingDown = true;
		}
	
	public DisplayController display;
	public KeyboardController keyboard;
	public MouseController mouse;
	public DiskController disk;
	
	public boolean isShuttingDown = false;

	}
