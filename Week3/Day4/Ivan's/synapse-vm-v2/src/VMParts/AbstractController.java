package VMParts;

// Here I define an abstract controller that contains code that
// all Controllers should have. I use it as a base for inhertience. 
// ie> common code for all controllers will appear here. 
public abstract class AbstractController 
	{
	  public abstract void processController();
	  public  boolean doShutdownProcess()
	  	{
		 return true;
	  	}
	
	}
