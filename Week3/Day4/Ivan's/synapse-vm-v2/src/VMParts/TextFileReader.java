package VMParts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.FileReader;
import java.io.BufferedReader;


public class TextFileReader
	{
	public static ArrayList readTextFileUsingFileReader(String fileName) 
		{
		ArrayList<String> alist=new ArrayList<String>();
		
	    try {
	    	FileReader textFileReader = new FileReader(fileName);
	      
	    	char[] buffer = new char[8096];
	    	
	    	int numberOfCharsRead = textFileReader.read(buffer);
	      
	    	while (numberOfCharsRead != -1) 
	      		{
	    		alist.add(String.valueOf(buffer, 0, numberOfCharsRead));
	    		System.out.println(String.valueOf(buffer, 0, numberOfCharsRead));
	    		numberOfCharsRead = textFileReader.read(buffer);
	      		}
	    	textFileReader.close();
	      
	    	} 
	    catch (IOException e) 
	    	{
	    	// TODO Auto-generated catch block
	    	e.printStackTrace();
	    	}
	    return alist;
	  	}
	}

