import java.util.*;
import java.util.concurrent.TimeUnit;

public class MathGame {

    public static String[] operand = {"+","-","*","/"};
    public static boolean contQuestion = true;
    public static final int countdownVal = 5;
    //final static Timer timer = new Timer();
    public static int time;
    int i = 5;

    public static String generateRandomOp(){
        Random r = new Random();
        return operand[r.nextInt(4)];
    }

    public static int generateRandomNum(){
        Random r = new Random();
        return r.nextInt(10);
    }

    public void customTimer(){
        int time;

        while(i>0){
            time = i;
            System.out.println(""+time);
            i-=1;
            try{
                TimeUnit.MILLISECONDS.sleep(1000);
                
            }catch(InterruptedException ex) {
                //
            }
            if(i==0){
                System.out.println("BYE");
            }
        }
    }
    public static void playingField(){
        Scanner input = new Scanner(System.in);
        MathGame m = new MathGame();
        
        while(contQuestion){
            double number1 = generateRandomNum();
            double number2 = generateRandomNum();
            String op = generateRandomOp();
            double cpAnswer = 0.0;

            switch (op) {

                case ("+"):
                cpAnswer = (number1 + number2);
                    break;

                case ("-"):
                cpAnswer = (number1 - number2);
                    break;

                case ("x"):
                cpAnswer = (number1 * number2);
                    break;

                case ("/"):
                cpAnswer = (number1 / number2);
                    break;
            }

            System.out.println("What is " +number1+ " " +op+ " " +number2+ " ?");
            
            //START TIMER
            while(m.i>0){
                time = m.i;
                System.out.println(""+time);
                m.i-=1;
                try{
                    TimeUnit.MILLISECONDS.sleep(1000);
                    
                }catch(InterruptedException ex) {
                    //
                }
                if(m.i==0){
                    System.out.println("BYE");
                }
            }

            double answer = input.nextDouble();


            if(answer == cpAnswer){
                System.out.println(+number1+ " " +op+ " " +number2+ " = " +answer+ " is true");
                m.i=5;
            }else{
                System.out.println("Wrong!");
                m.i=5;
            }
            
        }
        input.close();
        
    }

    public static void main(String[] args) {
        playingField();
    }
}