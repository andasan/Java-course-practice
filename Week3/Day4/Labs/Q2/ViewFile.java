import java.io.*;
public class ViewFile {
    
    //multiple file name handler
    public static void processHandler(String[] args) {

        for(int i=0; i< args.length;i++){
            String filename = args[i];
            File file = new File(filename);
            try{
                // Creates a FileReader Object
                FileReader fr = new FileReader(file); 
                char[] buffer = new char[16384];
                fr.read(buffer);   // reads the content to the array
                
                for(char c : buffer)
                    System.out.print(c);   // prints the characters one by one
                fr.close();
            }catch (IOException e) {
                System.out.println(e);
            }
        }
    }

    //process only one file
    // public static void processHandler(String[] args) {
    //     String filename = args[0];
    //     File file = new File(filename);
    //     try{
    //         // Creates a FileReader Object
    //         FileReader fr = new FileReader(file); 
    //         char[] buffer = new char[16384];
    //         fr.read(buffer);   // reads the content to the array
            
    //         for(char c : buffer)
    //             System.out.print(c);   // prints the characters one by one
    //         fr.close();
    //     }catch (IOException e) {
    //         System.out.println(e);
    //     }
    // }

    private static boolean hasParamaters(String[] args){
        return (args.length > 0);
    }

    public static void main(String[] args) {
        //Validations before passing to processHandler()
        if (hasParamaters (args) == true){
            processHandler(args);
        }
        else{
            System.out.println("You didn't enter anything");
        }
    }
}