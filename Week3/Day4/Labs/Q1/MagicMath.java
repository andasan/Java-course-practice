import java.util.*;
public class MagicMath {

    Scanner input = new Scanner(System.in);
    Cards cards = new Cards();
    int sum = 0;

    private void levels(int level){
        System.out.println("Is your number in this table? [Y/N]: ");
        int count = 0;
        for(int i=0; i<5;i++) {
            for (int j = count; j < 3+count; j++){
                System.out.printf("%4d",cards.table[level][j]);
            }
            count+=3;
            System.out.println();
        }
        String userResponse = input.next();
        if(userResponse.equals("Y") || userResponse.equals("y")){
            System.out.println();
            sum+=cards.table[level][0];
        }else if(userResponse.equals("N") || userResponse.equals("n")){
            System.out.println();
        }else{
            System.out.println("Invalid input");
        }
    }

    private void startGame(){
        System.out.println("Let me guess your number!!!");
        System.out.printf("Pick a Number Between 1 and 30\n.\n\n.\n");
        System.out.print("Press Y to continue...");
        String userResponse = input.next();
        if(userResponse.equals("Y") || userResponse.equals("y")){
            System.out.println();
            levels(0);
        }
        levels(1);
        levels(2);
        levels(3);
        levels(4);
        System.out.println("The number you had in mind is: "+sum);
    }
    public static void main(String[] args) {
        MagicMath magicMath = new MagicMath();
        magicMath.startGame();
    }

}