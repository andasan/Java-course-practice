public class Show {
    private int[][] seats;
    private final int SEATS_PER_ROW = 10; 
    private final int NUM_ROWS = 20;

    public boolean isAisleSeat (int row, int seatNumber){ 
        return seatNumber == 0 || seatNumber == SEATS_PER_ROW - 1; 
    }
     
    public boolean twoTogether(){ 
        for (int r = 0; r < NUM_ROWS; r++){
            for (int c = 0; c < SEATS_PER_ROW-1; c++){
                seats[r][c] = 1;
                seats[r][c+1] = 1;
                return true;
            }
        }
        if (seats[r][c] == 0 && seats[r][c+1] == 0){
            return false; 
        }
    }
    public int findAdjacent(int row, int seatsNeeded){
        int index = 0, count = 0, lowIndex = 0;
         
        while (index < SEATS_PER_ROW){
            while (index < SEATS_PER_ROW && seats[row][index] == 0){
                count++;
                index++;
                if (count == seatsNeeded)
                    return lowIndex;
            }
            count = 0;
            index++;
            lowIndex = index;
        }
        return -1; 
    }
}
           