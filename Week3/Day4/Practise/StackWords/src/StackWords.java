import java.util.Scanner;

public class StackWords {
    public static void stackWords() {
        Scanner input = new Scanner(System.in);
        String word = input.next(); //read user input
        if (word.equals(".")) //if user input is "."
            System.out.println();
        else //all words will be stacked on top of each other
            stackWords();
        System.out.println(word);
        input.close();
    }

    public static void main(String args[]) {
        System.out.println("Enter list of words, one per line.");
        System.out.println("Final word should be a period (.)");
        stackWords();
    }
}
