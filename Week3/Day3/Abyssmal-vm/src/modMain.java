public class modMain {
    public static boolean terminateProgram = false;
    public static modVM vm;

    public static void powerOn(modVM veeEm){
        veeEm.display = new modDisplay(1024,768);
        veeEm.keyboard = new modKeyboard();
        veeEm.mouse = new modMouse();
        veeEm.disk = new modDisk();
    }
    public static void powerOff(modVM veeEm){
        veeEm.isShuttingDown = true;
    }

    public static void main(String[] args) {
        terminateProgram = false;
        powerOn(vm);

        do{
            if(vm.isShuttingDown == false) {
                vm.display.processController();
                vm.keyboard.processController();
                vm.mouse.processController();
                vm.disk.processController();
            }else{
                vm.disk.doShutDownProcess();
                vm.display.doShutDownProcess();
                vm.keyboard.doShutDownProcess();
                vm.mouse.doShutDownProcess();
                terminateProgram = true;
            }

        }while(terminateProgram == false);
        powerOff(vm);

    }
}
