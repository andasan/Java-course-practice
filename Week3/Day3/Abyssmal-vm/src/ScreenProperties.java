public class ScreenProperties {
    public int screenWidth = 0; // this represents the width (in chars) for current screen
    public int screenHeight = 0; // this represents the height (in chars) for current screen

    public void setSize(int width, int height){
        screenWidth = width;
        screenHeight = height;
    }
}
