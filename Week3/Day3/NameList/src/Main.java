import java.io.*;
import java.util.*;
public class Main {

    private static File firstNameSource = new File("src/first_names_all.txt");
    private static File lastNameSource = new File("src/last_names_all.txt");
    public static ArrayList<String> lastNameList = new ArrayList<>();
    public static ArrayList<String> firstNameList = new ArrayList<>();

    //Generate a random number(index)
    private static int getRandomNumber(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min)+1) + min;
    }

    //Create a file and don't overwrite existing file
    public static void writeToFileNoOverwrite(ArrayList fullNames){
        File file = new File("src/GeneratedNames.txt");
        try {
            //(new FileWriter(file, true))
            PrintWriter output = new PrintWriter(new FileWriter(file, true)); //will not overwrite
//            for (String o:fullNames)
//                output.println(o);
            for(int x=0; x<fullNames.size();x++) {
                output.println(fullNames.get(x));
            }
            output.close();
            System.out.println("Successfully printed "+fullNames.size()+" names.");
        }
        catch (IOException e) {
            System.out.println("File does not exist on the File System");
        }
    }

    //Create a file and overwrite existing file
    public static void writeToFile(ArrayList fullNames){
        File file = new File("src/GeneratedNames.txt");
        try {
            PrintWriter output = new PrintWriter(file);
//            for (String o:fullNames)
//                output.println(o);
            for(int x=0; x<fullNames.size();x++) {
                output.println(fullNames.get(x));
            }
            output.close();
            System.out.println("Successfully printed "+fullNames.size()+" names.");
        }
        catch (FileNotFoundException e) {
            System.out.println("File does not exist on the File System");
        }
    }

    //Check if file exists and ask for a response to overwrite or not
    public static void beforeWriteToFile(ArrayList fullNames){
        File file = new File("src/GeneratedNames.txt");
        boolean keepAsking = true;
        if(file.exists()){
            System.out.println("File already exists");
            Scanner input = new Scanner(System.in);

            while(keepAsking){
                System.out.print("Do you want to overwrite this file? [Y/N]: ");

                String responseToWrite = input.next();
                System.out.println();

                if(responseToWrite.equals("Y") || responseToWrite.equals("y")){
                    writeToFile(fullNames);
                    keepAsking = false;
                }else if(responseToWrite.equals("N") || responseToWrite.equals("n")){
                    writeToFileNoOverwrite(fullNames);
                    keepAsking = false;
                }else{
                    System.out.println("You didn't enter the right response. Try again.");
                    //System.exit(1);
                }
            }
        }
    }

    //Create new file and combine both ArrayList
    public static void combineNames(int numberOfNames){
        ArrayList<String> fullNames = new ArrayList<>();

        System.out.print("Printing ");
        for(int x=0; x<numberOfNames; x++) {
            fullNames.add(LastName() + " " + FirstName());
            System.out.print(". ");
        }
        System.out.println();
        beforeWriteToFile(fullNames);

    }

    //Process last name
    public static String LastName(){
        try {
            Scanner input = new Scanner(lastNameSource);
            while (input.hasNext()){
                lastNameList.add(input.next());
            }
            input.close();
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist on the File System");
        }
        return lastNameList.get(getRandomNumber(0,lastNameList.size())); //return a random last name
    }

    //Process first name
    public static String FirstName(){
        try {
            Scanner input = new Scanner(firstNameSource);
            while (input.hasNext()){
                firstNameList.add(input.next().toUpperCase());
            }
            input.close();
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist on the File System");
        }
        return firstNameList.get(getRandomNumber(0,firstNameList.size())); //return a random first name
    }

    //Process method
    public static void processHandler(){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a number of names to print: ");
        int numberOfNames = input.nextInt();
        if(numberOfNames > 0){
            combineNames(numberOfNames);
        }
        else{
            System.out.println("OKay.... Maybe next time.");
        }
        input.close();
    }

    public static void main(String[] args) {

        //Validations before passing to processHandler()

        if(!firstNameSource.canRead() && !lastNameSource.canRead()){ //if both list cant be read
            System.out.println("Unable to read the file");
        }else if(!firstNameSource.canWrite() && !lastNameSource.canWrite()){ //if both list cant be overwritten
            System.out.println("Unable to write on this file");
        }else{
            processHandler();
        }

    }
}
