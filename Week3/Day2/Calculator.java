public class Calculator {

    private static int checkOperand(String op, int num1, int num2){
        int answer = 0;
        switch (op) {

            case ("+"):
                answer = (num1 + num2);
                break;

            case ("-"):
                answer = (num1 - num2);
                break;

            case ("x"):
                answer = (num1 * num2);
                break;

            case ("/"):
                answer = (num1 / num2);
                break;
        }
        return answer;
    }
    public void doCalculation(String[] args){
        int num1 = Integer.parseInt(args[0]);
        int num2 = Integer.parseInt(args[2]);
        String op = args[1];

        System.out.println(checkOperand(op, num1, num2));
    }

    private static boolean hasParamaters(String[] args){
        return (args.length > 0);
    }

    public static void main(String[] args) {
        if (hasParamaters (args) == true){
            Calculator calc = new Calculator();
            calc.doCalculation(args);
        }
        else{
            System.out.println("You didn't enter anything");
        }
    }
}