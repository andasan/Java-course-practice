import java.util.Arrays;
import java.util.ArrayList;
import java.util.Random;

/**
 * generateRandomNumber
 */
public class GenerateRandomNumber {

    //method to generate random numbers
    private static int getRandomNumber(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min)-1) + min+1;    //will exclude min and max numbers form the range
    }

    private static int[] compileRandomNumbers(int minNum, int maxNum, int numOfLoops){
        int[] result = new int[numOfLoops];
        int newRandom; 
        ArrayList<Integer> repeatedList = new ArrayList<Integer>();

        for (int i = 0; i < numOfLoops; i++) {  
            do {  
                newRandom = getRandomNumber(minNum, maxNum);
            } while (repeatedList.contains(newRandom));  //if a random number exists in repeatedList, loop again
            result[i] = newRandom;  
            repeatedList.add(newRandom);  
        } 

        return result;

    }

    private static void processCommandLine(String[] args){
        int minNum = Integer.parseInt(args[0]);
        int maxNum = Integer.parseInt(args[1]);
        int numOfLoops = Integer.parseInt(args[2]);
        int[] result = compileRandomNumbers(minNum, maxNum, numOfLoops);

        Arrays.sort(result);    //sort in ascending order ---- cosmetics 
        System.out.println("Result: "+Arrays.toString(result));
    }

    public static boolean rangeIsLargerThanLoops(String[] args){
        int minNum = Integer.parseInt(args[0]);
        int maxNum = Integer.parseInt(args[1]);
        int numOfLoops = Integer.parseInt(args[2]);

        //for cases like 1 1 or 1 2
        if((maxNum-minNum) == 0 || minNum+1 == maxNum)
            throw new IllegalArgumentException("No possibility to generate a random number between the range");
        
        //for cases like  5 1 2
        if (minNum >= maxNum)
            throw new IllegalArgumentException("Max must be greater than min");

        return (numOfLoops > (maxNum-minNum));
    }

    public static boolean isInt(String[] str) {
        try {
            Integer.parseInt(str[0]);
            Integer.parseInt(str[1]);
            Integer.parseInt(str[2]);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    private static boolean hasParamaters(String[] args){
        return (args.length > 0);
    }

    public static void main(String[] args) {
        //check for empty params
        if (hasParamaters (args) == true){
            //check if params is an int or....
            if(isInt (args) == true){
                //check if iterations is more than the range between min and max
                if(rangeIsLargerThanLoops (args) == false){
                    processCommandLine(args);
                }
                else{
                    //for cases like 1 5 50
                    System.out.println("Can't generate random numbers more than your range of given numbers");
                }
            }
            else{
                //string or character entry
                System.out.println("You need to enter integers");
            }
        }
        else{
            //empty entry
            System.out.println("You didn't enter anything");
        }
    }
}