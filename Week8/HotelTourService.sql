-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Aug 11, 2019 at 11:11 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `HotelTourService`
--

-- --------------------------------------------------------

--
-- Table structure for table `City`
--

CREATE TABLE `City` (
  `id` int(11) NOT NULL,
  `CityName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `City`
--

INSERT INTO `City` (`id`, `CityName`) VALUES
(1, 'Vancouver'),
(2, 'North Vancouver'),
(3, 'Burnaby'),
(4, 'Richmond'),
(5, 'Ottawa'),
(6, 'Toronto'),
(7, 'Quebec city'),
(8, 'Calgary'),
(9, 'New York city'),
(10, 'Washington DC'),
(11, 'Shinjuku'),
(12, 'Nagoya'),
(13, 'Osaka'),
(14, 'Fukuoka'),
(15, 'Jiuhun'),
(16, 'Taipei'),
(17, 'Central District'),
(18, 'Yancheng'),
(19, 'Xitun District'),
(20, 'Manila'),
(21, 'Legazpi'),
(22, 'Puerto Princesa');

-- --------------------------------------------------------

--
-- Table structure for table `Comments`
--

CREATE TABLE `Comments` (
  `commentID` int(11) NOT NULL,
  `touristID` int(11) DEFAULT NULL,
  `POI_id` int(11) NOT NULL,
  `otherComments` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Comments`
--

INSERT INTO `Comments` (`commentID`, `touristID`, `POI_id`, `otherComments`, `created_at`, `modified_at`) VALUES
(1, 2, 5, 'This activity is sooooooooo fun.', '2019-08-07 08:52:59', '2019-08-08 08:53:29'),
(2, 3, 4, 'Very unique restaurant!', '2019-08-07 08:52:59', '2019-08-08 08:53:29'),
(3, 4, 1, 'Excellent environment', '2019-08-07 08:52:59', '2019-08-08 08:53:29'),
(4, 1, 4, 'I love the wine! ', '2019-08-07 08:52:59', '2019-08-08 08:53:29'),
(5, 5, 6, 'Friendly service, thank you!', '2019-08-07 08:52:59', '2019-08-08 08:53:29');

-- --------------------------------------------------------

--
-- Table structure for table `Country`
--

CREATE TABLE `Country` (
  `id` int(11) NOT NULL,
  `CountryName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Country`
--

INSERT INTO `Country` (`id`, `CountryName`) VALUES
(1, 'Canada'),
(2, 'USA'),
(3, 'Japan'),
(4, 'Taiwan'),
(5, 'Philippines');

-- --------------------------------------------------------

--
-- Table structure for table `Hotel_Admin`
--

CREATE TABLE `Hotel_Admin` (
  `HotelAdminID` int(11) NOT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `Mobile` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `City` int(11) NOT NULL,
  `State` int(11) NOT NULL,
  `Country` int(11) NOT NULL,
  `UserName` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Role` int(11) NOT NULL,
  `profileIMG` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Hotel_Admin`
--

INSERT INTO `Hotel_Admin` (`HotelAdminID`, `FirstName`, `LastName`, `Mobile`, `Email`, `City`, `State`, `Country`, `UserName`, `Password`, `Role`, `profileIMG`) VALUES
(1, 'John ', 'Doe', '999-999-9999', '12345@gmail', 1, 1, 1, 'John', '123456johnDoe', 1, ''),
(2, 'Sam', 'Smith', '888-888-8888', '5678@gmail.com', 1, 1, 1, 'Sam', '56789Sam', 1, ''),
(3, 'James', 'Bond', '777-777-7777', '13579@gmail.com', 1, 1, 1, 'James', '24680James', 1, ''),
(4, 'Philip', 'Morris', '235-111-5678', 'pm@gmail.com', 3, 1, 1, 'test', 'test', 1, 'profilepic.png');

-- --------------------------------------------------------

--
-- Table structure for table `POI`
--

CREATE TABLE `POI` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `City` int(11) NOT NULL,
  `State` int(11) NOT NULL,
  `Country` int(11) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `EntryPrice` float NOT NULL,
  `POI_requirements` int(11) NOT NULL,
  `POI_Rating` int(11) NOT NULL,
  `img_src` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `POI`
--

INSERT INTO `POI` (`id`, `category`, `Name`, `Description`, `City`, `State`, `Country`, `Address`, `EntryPrice`, `POI_requirements`, `POI_Rating`, `img_src`) VALUES
(1, 5, 'Capilano Suspension Bridge', 'The Capilano Suspension Bridge is a simple suspension bridge crossing the Capilano River in the District of North Vancouver, British Columbia, Canada. The current bridge is 140 metres long and 70 metres above the river.', 1, 1, 1, '3735 Capilano Rd, North Vancouver, BC V7R 4J1', 4.5, 1, 5, 'capilano.jpg'),
(2, 3, 'Lynn Canyon Park', 'Lynn Canyon Park is a municipal park in the District of North Vancouver, British Columbia. When the park officially opened in 1912 it was only 12 acres in size, but it now encompasses 617 acres. The park has many hiking trails of varying length and difficulty.', 2, 1, 1, '3690 Park Rd, North Vancouver, BC V7J 3K2', 5, 1, 5, 'lynn_canyon_park.jpg'),
(3, 3, 'Deep Cove', 'Deep Cove refers to the community in the easternmost part of the District of North Vancouver, in British Columbia, Canada, and is also the geographic name of the small bay beside the town. It is affectionately referred to as \"The Cove\" by local residents.', 2, 1, 1, 'British Columbia, Canada', 8, 1, 4, 'deep_cove.jpg'),
(4, 5, 'Sushi Saito', 'Sushi Saito is a three Michelin star Japanese cuisine restaurant in Minato, Tokyo, primarily known for serving Sushi. ', 11, 10, 3, 'Japan, 〒106-0032 Tokyo, Minato City, Roppongi, 1 Chome−4−５, ARK Hills South Tower, 1F', 0, 3, 4, 'sushi_saito.jpg'),
(5, 4, 'Godzilla Head', 'Massive Godzilla head sitting atop a movie complex run by Toho, the studio behind the franchise.', 11, 10, 3, '〒160-0021, 1 Chome-18-8 Kabukicho, Shinjuku City, Tokyo 160-0021, Japan', 0, 2, 5, 'godzilla_head_tokyo'),
(6, 2, 'Taipei 101', 'The Taipei 101 formerly known as the Taipei World Financial Center, is a supertall skyscraper designed by C.Y. Lee & Partners and built by KTRT Joint Venture in Xinyi, Taipei, Taiwan', 16, 14, 4, 'No. 7 Xin Yi Road, District Xin Yi', 300, 2, 3, 'Taipei101');

-- --------------------------------------------------------

--
-- Table structure for table `POI_Category`
--

CREATE TABLE `POI_Category` (
  `id` int(11) NOT NULL,
  `CatName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `POI_Category`
--

INSERT INTO `POI_Category` (`id`, `CatName`) VALUES
(1, 'Adventure Tour'),
(2, 'Family Tour'),
(3, 'National Parks Tour'),
(4, 'Religous Tour'),
(5, 'Food Tour');

-- --------------------------------------------------------

--
-- Table structure for table `POI_Requirements`
--

CREATE TABLE `POI_Requirements` (
  `id` int(11) NOT NULL,
  `ReqName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `POI_Requirements`
--

INSERT INTO `POI_Requirements` (`id`, `ReqName`) VALUES
(1, 'Bring Insect repellant'),
(2, 'Bring your Camera'),
(3, 'Empty your palate');

-- --------------------------------------------------------

--
-- Table structure for table `Roles`
--

CREATE TABLE `Roles` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Roles`
--

INSERT INTO `Roles` (`id`, `Name`) VALUES
(1, 'Admin'),
(2, 'Moderator'),
(3, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `State`
--

CREATE TABLE `State` (
  `id` int(11) NOT NULL,
  `StateName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `State`
--

INSERT INTO `State` (`id`, `StateName`) VALUES
(1, 'British Columbia'),
(2, 'Ontario'),
(4, 'Quebec'),
(5, 'Alberta'),
(6, 'New York'),
(7, 'Columbia'),
(8, 'California'),
(9, 'Washington'),
(10, 'Tokyo'),
(11, 'Nagoya'),
(12, 'Osaka'),
(13, 'Fukuoka'),
(14, 'New Taipei city'),
(15, 'Taichung City'),
(16, 'Kaohsiung City'),
(17, 'Taipei city'),
(18, 'Metro Manila'),
(19, 'Albay'),
(20, 'Palawan'),
(21, 'Ruifang District');

-- --------------------------------------------------------

--
-- Table structure for table `Tourist`
--

CREATE TABLE `Tourist` (
  `touristID` int(11) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `Mobile` int(11) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `City` int(11) NOT NULL,
  `State` int(11) NOT NULL,
  `Country` int(11) NOT NULL,
  `PassportNumber` varchar(255) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Tourist`
--

INSERT INTO `Tourist` (`touristID`, `FirstName`, `LastName`, `Mobile`, `Email`, `City`, `State`, `Country`, `PassportNumber`, `Username`, `Password`, `Role`) VALUES
(1, 'Aaron', 'Smith', 778000000, 'aaron@gmail.com', 1, 1, 1, 'JJ100000', 'aaronamith', 'aaron00', 3),
(2, 'Barbra', 'Johnson', 778111111, 'barbra@gmail.com', 15, 21, 4, 'EB111111', 'barbrajohnson', 'barbra00', 3),
(3, 'Cavalon', 'Williams', 778222222, 'cavanlon@gmail.com', 6, 2, 1, 'JL222222', 'cavalonwilliams', 'cavalon00', 3),
(4, 'Delsie', 'Brown', 778333333, 'delsie@gmail.com', 12, 11, 3, 'TR333333', 'delsiebrown', 'delsie00', 3),
(5, 'Evelyn', 'Miller', 778444444, 'evelyn@gmail.com', 22, 20, 5, 'GG444444', 'evelynmiller', 'evelyn00', 3),
(6, 'Mark', 'Jello', 233447554, 'markjello@gmail.com', 1, 1, 1, 'Eb335532', 'mark', 'mark', 3),
(7, 'test', 'test', 1233, 'test@mail', 1, 1, 1, 'wetwt3434', 'test1', 'test1', 3),
(8, 'wefwdf ', 'we rwf', 56456, 'wegg', 2, 1, 1, 'wegb', 'dgn', 'wehh', 3),
(9, 'erhrny', 'vwetwb', 346347, 'ebtenn', 2, 4, 3, 'eryb', 'weteb', 'wetey', 3),
(10, 'Lily', 'Flower', 999, 'flower.lily@mail.com', 21, 19, 5, 'PP12345', 'lily', 'flower', 3),
(11, 'Mark', 'Bautista', 122334455, 'mark@mail', 19, 17, 4, 'EB333445', 'mark', 'bau', 3);

-- --------------------------------------------------------

--
-- Table structure for table `Tourist_POI_Rating`
--

CREATE TABLE `Tourist_POI_Rating` (
  `RatingID` int(11) NOT NULL,
  `Rating_val` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Tourist_POI_Rating`
--

INSERT INTO `Tourist_POI_Rating` (`RatingID`, `Rating_val`) VALUES
(1, 'Poor'),
(2, 'Fair'),
(3, 'Good'),
(4, 'Excellent'),
(5, 'Extraordinary');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `City`
--
ALTER TABLE `City`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Comments`
--
ALTER TABLE `Comments`
  ADD PRIMARY KEY (`commentID`),
  ADD KEY `FK_COMMENT_TOURISTID` (`touristID`),
  ADD KEY `FK_COMMENT_POI` (`POI_id`);

--
-- Indexes for table `Country`
--
ALTER TABLE `Country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Hotel_Admin`
--
ALTER TABLE `Hotel_Admin`
  ADD PRIMARY KEY (`HotelAdminID`),
  ADD KEY `FK_ADMIN_CITY` (`City`),
  ADD KEY `FK_ADMIN_STATE` (`State`),
  ADD KEY `FK_ADMIN_ROLE` (`Role`),
  ADD KEY `FK_ADMIN_COUNTRY` (`Country`);

--
-- Indexes for table `POI`
--
ALTER TABLE `POI`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_POI_REQUIREMENTS` (`POI_requirements`),
  ADD KEY `FK_POI_CATEGORY` (`category`),
  ADD KEY `FK_POI_CITY` (`City`),
  ADD KEY `FK_POI_STATE` (`State`),
  ADD KEY `FK_POI_COUNTRY` (`Country`),
  ADD KEY `FK_POI_RATING` (`POI_Rating`);

--
-- Indexes for table `POI_Category`
--
ALTER TABLE `POI_Category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `POI_Requirements`
--
ALTER TABLE `POI_Requirements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Roles`
--
ALTER TABLE `Roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `State`
--
ALTER TABLE `State`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Tourist`
--
ALTER TABLE `Tourist`
  ADD PRIMARY KEY (`touristID`),
  ADD KEY `FK_TOURIST_STATE` (`State`),
  ADD KEY `FK_TOURIST_ROLE` (`Role`),
  ADD KEY `FK_TOURIST_CITY` (`City`),
  ADD KEY `FK_TOURIST_COUNTRY` (`Country`);

--
-- Indexes for table `Tourist_POI_Rating`
--
ALTER TABLE `Tourist_POI_Rating`
  ADD PRIMARY KEY (`RatingID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Comments`
--
ALTER TABLE `Comments`
  ADD CONSTRAINT `FK_COMMENT_POI` FOREIGN KEY (`POI_id`) REFERENCES `POI` (`id`),
  ADD CONSTRAINT `FK_COMMENT_TOURISTID` FOREIGN KEY (`touristID`) REFERENCES `tourist` (`touristID`);

--
-- Constraints for table `Hotel_Admin`
--
ALTER TABLE `Hotel_Admin`
  ADD CONSTRAINT `FK_ADMIN_CITY` FOREIGN KEY (`City`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `FK_ADMIN_COUNTRY` FOREIGN KEY (`Country`) REFERENCES `Country` (`id`),
  ADD CONSTRAINT `FK_ADMIN_ROLE` FOREIGN KEY (`Role`) REFERENCES `Roles` (`id`),
  ADD CONSTRAINT `FK_ADMIN_STATE` FOREIGN KEY (`State`) REFERENCES `state` (`id`);

--
-- Constraints for table `POI`
--
ALTER TABLE `POI`
  ADD CONSTRAINT `FK_POI_CATEGORY` FOREIGN KEY (`category`) REFERENCES `POI_Category` (`id`),
  ADD CONSTRAINT `FK_POI_CITY` FOREIGN KEY (`City`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `FK_POI_COUNTRY` FOREIGN KEY (`Country`) REFERENCES `country` (`id`),
  ADD CONSTRAINT `FK_POI_RATING` FOREIGN KEY (`POI_Rating`) REFERENCES `tourist_poi_rating` (`RatingID`),
  ADD CONSTRAINT `FK_POI_REQUIREMENTS` FOREIGN KEY (`POI_requirements`) REFERENCES `POI_Requirements` (`id`),
  ADD CONSTRAINT `FK_POI_STATE` FOREIGN KEY (`State`) REFERENCES `state` (`id`);

--
-- Constraints for table `Tourist`
--
ALTER TABLE `Tourist`
  ADD CONSTRAINT `FK_TOURIST_CITY` FOREIGN KEY (`City`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `FK_TOURIST_COUNTRY` FOREIGN KEY (`Country`) REFERENCES `Country` (`id`),
  ADD CONSTRAINT `FK_TOURIST_ROLE` FOREIGN KEY (`Role`) REFERENCES `Roles` (`id`),
  ADD CONSTRAINT `FK_TOURIST_STATE` FOREIGN KEY (`State`) REFERENCES `state` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
