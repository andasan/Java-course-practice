
package org.hotel;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.events.JFXDialogEvent;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.net.URL;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.effect.BoxBlur;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author francoispolo
 */
public class Fx_mainController implements Initializable {
    
    private static String status = "";
    private static int Role = 0;
    private String UserFirstName = "";
    private String UserPic = "";
    
    @FXML private Button loginBtn;
    @FXML private Button registerBtn;
    @FXML private StackPane myStackPane;
    @FXML private AnchorPane rootAnchorPane;
    
    @FXML
    private void loadAdmin() throws IOException{
        Scene scene = myStackPane.getScene();
        //Parent root = FXMLLoader.load(getClass().getResource("fx_admin.fxml"));
        
        
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("fx_admin.fxml"));
        Parent root = (Parent)loader.load();
        
        Fx_adminController controller = loader.getController();
        controller.initData(UserFirstName,UserPic);
        
        
        
        root.translateYProperty().set(scene.getHeight());
        myStackPane.getChildren().add(root);
        
        Timeline timeline = new Timeline();
        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
        KeyFrame kf = new KeyFrame(Duration.seconds(1), kv);
        timeline.getKeyFrames().add(kf);
        timeline.setOnFinished(event->{
            myStackPane.getChildren().remove(rootAnchorPane);
        });
        timeline.play();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    @FXML
    private void loadDialog(ActionEvent event){
        BoxBlur blur = new BoxBlur(3, 3, 3);
        Scene scene = new Scene(new Group()); 
        
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.getStyleClass().add("mainclass");
        ((Group)scene.getRoot()).getChildren().add(dialogLayout);
        
        Label title = new Label("Login");
        
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(20, 10, 20, 10));

        TextField userNameField = new TextField();
        userNameField.setPromptText("Username");
        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("Password");
        
        gridPane.setHalignment(title, HPos.CENTER);
        gridPane.setValignment(title, VPos.TOP);
        gridPane.add(title, 0, 0, 2, 1);
        
        gridPane.add(userNameField, 0, 1);
        gridPane.add(passwordField, 1, 1);
        
        JFXButton button = new JFXButton("Submit");
        
        final Text actiontarget = new Text();
        
        JFXDialog dialog = new JFXDialog(myStackPane, dialogLayout, JFXDialog.DialogTransition.TOP);
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent mouseEvent) ->{
            actiontarget.setText("");
//            if(userNameField.getText() == null || userNameField.getText().trim().isEmpty()) System.out.print("Null val");
//            else dialog.close();
            if (passwordField.getText().equals("") && userNameField.getText().equals("")) {
                actiontarget.setFill(Color.RED);
                actiontarget.setText("Username and Password is required");
                gridPane.setHalignment(actiontarget, HPos.CENTER);
                gridPane.add(actiontarget, 0, 2, 2, 1);
            }
            if (userNameField.getText().equals("") && !passwordField.getText().equals("")) {
                actiontarget.setFill(Color.RED);
                actiontarget.setText("Username is required");
                gridPane.add(actiontarget, 0, 2);
            }
            if (passwordField.getText().equals("") && !userNameField.getText().equals("")) {
                actiontarget.setFill(Color.RED);
                actiontarget.setText("Password is required");
                gridPane.add(actiontarget, 0, 2);
            }
            // If both of the above textFields have values
            if (!passwordField.getText().equals("")
                    && !userNameField.getText().equals("")) {
                status = logIn(userNameField.getText(), passwordField.getText());
                if(status.equals("Error")){
                    actiontarget.setText("Incorrect credentials");
                    gridPane.add(actiontarget, 0, 2);
                }else if(status.equals("Success")){
                    dialog.close();
                    if(Role == 1) try {
                        loadAdmin();
                    } catch (IOException ex) {
                        Logger.getLogger(Fx_mainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        
        userNameField.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) -> {
            if (newPropertyValue) {
                // Clearing message if any
                actiontarget.setText("");
            }
        });

        passwordField.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) -> {
            if (newPropertyValue) {
                // Clearing message if any
                actiontarget.setText("");
            }
        });

        dialogLayout.setBody(gridPane);
        //dialogLayout.setHeading(new Text("Login"));
        dialogLayout.setActions(button);
        
        dialog.show();
        dialog.setOnDialogClosed((JFXDialogEvent event1) ->{
            rootAnchorPane.setEffect(null);
        });
        rootAnchorPane.setEffect(blur);
    }

    
    private String logIn(String txtUsername, String txtPassword) {
        String username = txtUsername;
        String password = txtPassword;

        Connection con;
        
        String sql1 = "SELECT * FROM Hotel_Admin WHERE username = '"+username+"' AND password = '"+password+"'";
        String sql2 = "SELECT * FROM tourists WHERE username = '"+username+"' AND password = '"+password+"'";

        try {
            con = DBConnector.getConnection();
            ResultSet rs = con.createStatement().executeQuery(sql1);
            if (!rs.next()) {
                rs = con.createStatement().executeQuery(sql2);
                if (!rs.next()) {
                    return "Error";
                }
                else{
                    Role = rs.getInt("Role");
                    UserFirstName = rs.getString("FirstName");
                    UserPic = rs.getString("profileIMG");
                    
                    return "Success";
                }
            } else {
                Role = rs.getInt("Role");
                UserFirstName = rs.getString("FirstName");
                UserPic = rs.getString("profileIMG");
                
                return "Success";
            }

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return "Exception";
        }

    }
}
