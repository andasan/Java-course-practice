/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hotel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author francoispolo
 */
public class DBConnector {
    
    private static final String DATABASE_URL = "jdbc:mysql://localhost";
    private static final String DATABASE_PORT = "8889";
    private static final String DATABASE_NAME = "HotelTourService";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "root"; 
    
    public static Connection getConnection() throws SQLException {
        
        Connection connection = DriverManager.getConnection(DATABASE_URL+":"+DATABASE_PORT+"/"+DATABASE_NAME, DB_USERNAME, DB_PASSWORD);
        
        return connection;
        
    }
    
}
