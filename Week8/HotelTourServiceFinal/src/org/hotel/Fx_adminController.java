
package org.hotel;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.events.JFXDialogEvent;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.net.URL;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author francoispolo
 */
public class Fx_adminController implements Initializable {
    
    @FXML private TableView<ModelTouristTable> table;
    @FXML private TableColumn<ModelTouristTable, String> tour_col_firstName;
    @FXML private TableColumn<ModelTouristTable, String> tour_col_lastName;
    @FXML private TableColumn<ModelTouristTable, Integer> tour_col_mobile;
    @FXML private TableColumn<ModelTouristTable, String> tour_col_email;
    @FXML private TableColumn<ModelTouristTable, String> tour_col_city;
    @FXML private TableColumn<ModelTouristTable, String> tour_col_state;
    @FXML private TableColumn<ModelTouristTable, String> tour_col_country;
    @FXML private TableColumn<ModelTouristTable, String> tour_col_passport;
    
    @FXML private TableView<ModelPOITable> table_POI;
    @FXML private TableColumn<ModelPOITable, String> poi_col_category;
    @FXML private TableColumn<ModelPOITable, String> poi_col_poiName;
    @FXML private TableColumn<ModelPOITable, String> poi_col_desc;
    @FXML private TableColumn<ModelPOITable, String> poi_col_city;
    @FXML private TableColumn<ModelPOITable, String> poi_col_state;
    @FXML private TableColumn<ModelPOITable, String> poi_col_country;
    @FXML private TableColumn<ModelPOITable, String> poi_col_address;
    @FXML private TableColumn<ModelPOITable, Float> poi_col_price;
    @FXML private TableColumn<ModelPOITable, String> poi_col_req;
    @FXML private TableColumn<ModelPOITable, String> poi_col_rating;
    
    ObservableList<ModelTouristTable> oblist = FXCollections.observableArrayList();
    ObservableList<ModelPOITable> oblistPOI = FXCollections.observableArrayList();
    
    int maxID = 0;
    int POImaxID = 0;
    int cityID, stateID, countryID, POICatID, POIReqID, POIRatingID;
    
    @FXML private Button logoutBtn;
    @FXML private Button addUserBtn;
    @FXML private Label labelUserName;
    @FXML private ImageView profilePic;
    @FXML private StackPane myStackPaneAdmin;
    @FXML private AnchorPane rootAnchorPaneAdmin;
    
    @FXML
    private void logOut(ActionEvent event) throws IOException{
        makeFadeOut();
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        String sql = "SELECT *\n" +
                     "FROM tourist\n" +
                     "INNER JOIN City ON tourist.city = city.id\n"+
                     "INNER JOIN State ON tourist.state = state.id\n" +
                     "INNER JOIN Country ON tourist.country = country.id";
        String sqlPOI = "SELECT *\n" +
                     "FROM POI\n" +
                     "INNER JOIN POI_Category ON POI.category = POI_Category.id \n"+
                     "INNER JOIN POI_Requirements ON POI.POI_requirements = POI_Requirements.id \n"+
                     "INNER JOIN City ON POI.city = city.id\n"+
                     "INNER JOIN State ON POI.state = state.id\n" +
                     "INNER JOIN Country ON POI.country = country.id\n" +
                     "INNER JOIN Tourist_POI_Rating ON POI.POI_Rating = Tourist_POI_Rating.RatingID";
        
        Connection con;
        try {
            con = DBConnector.getConnection();
            ResultSet rs = con.createStatement().executeQuery(sql);
            while(rs.next()){
            oblist.add(new ModelTouristTable(rs.getString("FirstName"),
                                             rs.getString("LastName"),
                                             rs.getInt("Mobile"),
                                             rs.getString("Email"),
                                             rs.getString("CityName"),
                                             rs.getString("StateName"),
                                             rs.getString("CountryName"),
                                             rs.getString("PassportNumber")
                ));
            
            }
            con.close();
            con = DBConnector.getConnection();
            ResultSet rs2 = con.createStatement().executeQuery(sqlPOI);
            while(rs2.next()){
            oblistPOI.add(new ModelPOITable(rs2.getString("CatName"),
                                             rs2.getString("Name"),
                                             rs2.getString("Description"),
                                             rs2.getString("CityName"),
                                             rs2.getString("StateName"),
                                             rs2.getString("CountryName"),
                                             rs2.getString("Address"),
                                             rs2.getFloat("EntryPrice"),
                                             rs2.getString("ReqName"),
                                             rs2.getString("Rating_val")
                ));
            
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Fx_adminController.class.getName()).log(Level.SEVERE, null, ex);
        }

        tour_col_firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tour_col_lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        tour_col_mobile.setCellValueFactory(new PropertyValueFactory<>("mobile"));
        tour_col_email.setCellValueFactory(new PropertyValueFactory<>("email"));
        tour_col_city.setCellValueFactory(new PropertyValueFactory<>("city"));
        tour_col_state.setCellValueFactory(new PropertyValueFactory<>("state"));
        tour_col_country.setCellValueFactory(new PropertyValueFactory<>("country"));
        tour_col_passport.setCellValueFactory(new PropertyValueFactory<>("passport"));
        table.setItems(oblist);
    
        poi_col_category.setCellValueFactory(new PropertyValueFactory<>("category"));
        poi_col_poiName.setCellValueFactory(new PropertyValueFactory<>("name"));
        poi_col_desc.setCellValueFactory(new PropertyValueFactory<>("description"));
        poi_col_city.setCellValueFactory(new PropertyValueFactory<>("city"));
        poi_col_state.setCellValueFactory(new PropertyValueFactory<>("state"));
        poi_col_country.setCellValueFactory(new PropertyValueFactory<>("country"));
        poi_col_address.setCellValueFactory(new PropertyValueFactory<>("address"));
        poi_col_price.setCellValueFactory(new PropertyValueFactory<>("price"));
        poi_col_req.setCellValueFactory(new PropertyValueFactory<>("poi_requirements"));
        poi_col_rating.setCellValueFactory(new PropertyValueFactory<>("rating"));
        table_POI.setItems(oblistPOI);
    }    
    
    public void initData(String UserFirstName, String UserPic){
        labelUserName.setText("Hi, "+UserFirstName);
        Image image = new Image(getClass().getResourceAsStream("img/"+UserPic));
        profilePic.setFitHeight(150);
        profilePic.setFitWidth(150);
        profilePic.setImage(image);
    }
    
    public int getID(String query, String col_name) throws SQLException{
        Connection con = DBConnector.getConnection();
        ResultSet rs = con.createStatement().executeQuery(query);
        rs.next();
        if(col_name.equals("Tourist_POI_Rating")) return rs.getInt("RatingID");
        return rs.getInt("id");
    }
    
    @FXML
    private void addUser(ActionEvent event) throws SQLException{
        
        String sqlCity = "SELECT * FROM city";
        String sqlState = "SELECT * FROM state";
        String sqlCountry = "SELECT * FROM country";
        String maxIDnumber = "SELECT MAX(touristID) FROM tourist";
        
        Connection con;
        
                
        BoxBlur blur = new BoxBlur(3, 3, 3);
        Scene scene = new Scene(new Group()); 
        
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.getStyleClass().add("adminclass");
        ((Group)scene.getRoot()).getChildren().add(dialogLayout);
        
        Label title = new Label("Add new user");
        
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        TextField FirstNameField = new TextField();
        TextField LastNameField = new TextField();
        TextField MobileField = new TextField();
        TextField EmailField = new TextField();
        ChoiceBox<String> CityField = new ChoiceBox<>();
        ChoiceBox<String> StateField = new ChoiceBox<>();
        ChoiceBox<String> CountryField = new ChoiceBox<>();
        TextField PassportNumber = new TextField();
        TextField Username = new TextField();
        TextField Password = new TextField();
        
        Label FirstNameT = new Label("First Name");
        Label LastNameT = new Label("Last Name");
        Label MobileT = new Label("Mobile Number");
        Label EmailT = new Label("Email");
        Label CityT = new Label("City");
        Label StateT = new Label("State");
        Label CountryT = new Label("Country");
        Label PassportNumberT = new Label("Passport");
        Label UsernameT = new Label("Username");
        Label PasswordT = new Label("Password");
        

        con = DBConnector.getConnection();
        ResultSet rs = con.createStatement().executeQuery(sqlCity);
        while(rs.next()){
            CityField.getItems().add(rs.getString("CityName"));
        }
        
        rs = con.createStatement().executeQuery(sqlState);
        while(rs.next()){
            StateField.getItems().add(rs.getString("StateName"));
        }
        
        rs = con.createStatement().executeQuery(sqlCountry);
        while(rs.next()){
            CountryField.getItems().add(rs.getString("CountryName"));
        }
        rs = con.createStatement().executeQuery(maxIDnumber);
        if (rs.next()) {
           maxID = rs.getInt(1);
        }
        
        gridPane.setHalignment(title, HPos.CENTER);
        gridPane.setValignment(title, VPos.TOP);
        gridPane.add(title, 0, 0, 2, 1);
        
        gridPane.add(FirstNameT, 0, 1);
        gridPane.add(FirstNameField, 1, 1);
        gridPane.add(LastNameT, 0, 2);
        gridPane.add(LastNameField, 1, 2);
        gridPane.add(MobileT, 0, 3);
        gridPane.add(MobileField, 1, 3);
        gridPane.add(EmailT, 0, 4);
        gridPane.add(EmailField, 1, 4);
        gridPane.add(CityT, 0, 5);
        gridPane.add(CityField, 1, 5);
        gridPane.add(StateT, 0, 6);
        gridPane.add(StateField, 1, 6);
        gridPane.add(CountryT, 0, 7);
        gridPane.add(CountryField, 1, 7);
        gridPane.add(PassportNumberT, 0, 8);
        gridPane.add(PassportNumber, 1, 8);
        gridPane.add(UsernameT, 0, 9);
        gridPane.add(Username, 1, 9);
        gridPane.add(PasswordT, 0, 10);
        gridPane.add(Password, 1, 10);
        
        
        JFXButton button = new JFXButton("Submit");
        
        JFXDialog dialog = new JFXDialog(myStackPaneAdmin, dialogLayout, JFXDialog.DialogTransition.TOP);
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent mouseEvent) ->{
            
            try {
                
                String cityIDsql = "SELECT * FROM city WHERE CityName ='"+CityField.getValue()+"'";
                String stateIDsql = "SELECT * FROM state WHERE StateName ='"+StateField.getValue()+"'";
                String countryIDsql = "SELECT * FROM country WHERE CountryName ='"+CountryField.getValue()+"'";
                
                            
                cityID = getID(cityIDsql, "City");
                stateID = getID(stateIDsql, "State");
                countryID = getID(countryIDsql, "Country");
                
                maxID++;
                
                String addQuery = "INSERT INTO tourist(touristID,FirstName,LastName,Mobile,Email,City,State,Country,PassportNumber,Username,Password,Role) VALUES ('"+maxID+"','"+FirstNameField.getText()+"','"+LastNameField.getText()+"','"+MobileField.getText()+"','"+EmailField.getText()+"','"+cityID+"','"+stateID+"','"+countryID+"','"+PassportNumber.getText()+"','"+Username.getText()+"','"+Password.getText()+"','3')";
                
                
                PreparedStatement ps;
                
                    ps = con.prepareStatement(addQuery);
                    ps.executeUpdate();
                    con.close();
                    dialog.close();
            } catch (SQLException ex) {
                Logger.getLogger(Fx_adminController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });

        dialogLayout.setBody(gridPane);
        //dialogLayout.setHeading(new Text("Login"));
        dialogLayout.setActions(button);
        
        dialog.show();
        dialog.setOnDialogClosed((JFXDialogEvent event1) ->{
            rootAnchorPaneAdmin.setEffect(null);
        });
        rootAnchorPaneAdmin.setEffect(blur);
    }
    
   
    @FXML
    private void addPOI(ActionEvent event) throws SQLException{
        
        String sqlCity = "SELECT * FROM city";
        String sqlState = "SELECT * FROM state";
        String sqlCountry = "SELECT * FROM country";
        String sqlCategory = "SELECT * FROM POI_Category";
        String sqlRequirements = "SELECT * FROM POI_Requirements";
        String sqlRating = "SELECT * FROM Tourist_POI_Rating";
        String maxIDnumber = "SELECT MAX(id) FROM POI";
        
        Connection con;
        
        BoxBlur blur = new BoxBlur(3, 3, 3);
        Scene scene = new Scene(new Group()); 
        
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.getStyleClass().add("adminclass");
        ((Group)scene.getRoot()).getChildren().add(dialogLayout);
        
        Label title = new Label("Add new POI");
        
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        ChoiceBox<String> CategoryField = new ChoiceBox<>();
        TextField POINameField = new TextField();
        TextField DescriptionField = new TextField();
        ChoiceBox<String> POICityField = new ChoiceBox<>();
        ChoiceBox<String> POIStateField = new ChoiceBox<>();
        ChoiceBox<String> POICountryField = new ChoiceBox<>();
        TextField AddressField = new TextField();
        TextField EntryPriceField = new TextField();
        ChoiceBox<String> POIRequirementsField = new ChoiceBox<>();
        ChoiceBox<String> POIRatingField = new ChoiceBox<>();
        
        
        Label CategoryFieldT = new Label("Category");
        Label POINameFieldT = new Label("Name");
        Label DescriptionFieldT = new Label("Description");
        Label POICityFieldT = new Label("City");
        Label POIStateFieldT = new Label("State");
        Label POICountryFieldT = new Label("Country");
        Label AddressFieldT = new Label("Address");
        Label EntryPriceFieldT = new Label("Entry Price");
        Label POIRequirementsFieldT = new Label("Requirements");
        Label POIRatingFieldT = new Label("Rating");
        

        con = DBConnector.getConnection();
        ResultSet rs = con.createStatement().executeQuery(sqlCity);
        while(rs.next()){
            POICityField.getItems().add(rs.getString("CityName"));
        }
        
        rs = con.createStatement().executeQuery(sqlState);
        while(rs.next()){
            POIStateField.getItems().add(rs.getString("StateName"));
        }
        
        rs = con.createStatement().executeQuery(sqlCountry);
        while(rs.next()){
            POICountryField.getItems().add(rs.getString("CountryName"));
        }
        
        rs = con.createStatement().executeQuery(sqlCategory);
        while(rs.next()){
            CategoryField.getItems().add(rs.getString("CatName"));
        }
        
        rs = con.createStatement().executeQuery(sqlRequirements);
        while(rs.next()){
            POIRequirementsField.getItems().add(rs.getString("ReqName"));
        }
        rs = con.createStatement().executeQuery(sqlRating);
        while(rs.next()){
            POIRatingField.getItems().add(rs.getString("Rating_val"));
        }
        rs = con.createStatement().executeQuery(maxIDnumber);
        if (rs.next()) {
           POImaxID = rs.getInt(1);
        }
        
        gridPane.setHalignment(title, HPos.CENTER);
        gridPane.setValignment(title, VPos.TOP);
        gridPane.add(title, 0, 0, 2, 1);
        
        gridPane.add(CategoryFieldT, 0, 1);
        gridPane.add(CategoryField, 1, 1);
        gridPane.add(POINameFieldT, 0, 2);
        gridPane.add(POINameField, 1, 2);
        gridPane.add(DescriptionFieldT, 0, 3);
        gridPane.add(DescriptionField, 1, 3);
        gridPane.add(POICityFieldT, 0, 4);
        gridPane.add(POICityField, 1, 4);
        gridPane.add(POIStateFieldT, 0, 5);
        gridPane.add(POIStateField, 1, 5);
        gridPane.add(POICountryFieldT, 0, 6);
        gridPane.add(POICountryField, 1, 6);
        gridPane.add(AddressFieldT, 0, 7);
        gridPane.add(AddressField, 1, 7);
        gridPane.add(EntryPriceFieldT, 0, 8);
        gridPane.add(EntryPriceField, 1, 8);
        gridPane.add(POIRequirementsFieldT, 0, 9);
        gridPane.add(POIRequirementsField, 1, 9);
        gridPane.add(POIRatingFieldT, 0, 10);
        gridPane.add(POIRatingField, 1, 10);
        
        
        JFXButton button = new JFXButton("Submit");
        
        JFXDialog dialog = new JFXDialog(myStackPaneAdmin, dialogLayout, JFXDialog.DialogTransition.TOP);
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent mouseEvent) ->{
            
            try {
                
                String cityIDsql = "SELECT * FROM city WHERE CityName ='"+POICityField.getValue()+"'";
                String stateIDsql = "SELECT * FROM state WHERE StateName ='"+POIStateField.getValue()+"'";
                String countryIDsql = "SELECT * FROM country WHERE CountryName ='"+POICountryField.getValue()+"'";
                String categoryIDsql = "SELECT * FROM POI_Category WHERE CatName ='"+CategoryField.getValue()+"'";
                String reqIDsql = "SELECT * FROM POI_Requirements WHERE ReqName ='"+POIRequirementsField.getValue()+"'";
                String ratingIDsql = "SELECT * FROM Tourist_POI_Rating WHERE Rating_val ='"+POIRatingField.getValue()+"'";
                
                            
                cityID = getID(cityIDsql, "City");
                stateID = getID(stateIDsql, "State");
                countryID = getID(countryIDsql, "Country");
                POICatID = getID(categoryIDsql, "POI_Category");
                POIReqID = getID(reqIDsql, "POI_Requirements");
                POIRatingID = getID(ratingIDsql, "Tourist_POI_Rating");
                
                POImaxID++;
                String addQuery = "INSERT INTO POI(id,category,Name,Description,City,State,Country,Address,EntryPrice,POI_Requirements,POI_Rating) VALUES ('"+POImaxID+"','"+POICatID+"','"+POINameField.getText()+"','"+DescriptionField.getText()+"','"+cityID+"','"+stateID+"','"+countryID+"','"+AddressField.getText()+"','"+EntryPriceField.getText()+"','"+POIReqID+"','"+POIRatingID+"')";
                
                PreparedStatement ps;
                
                    ps = con.prepareStatement(addQuery);
                    ps.executeUpdate();
                    con.close();
                    dialog.close();
            } catch (SQLException ex) {
                Logger.getLogger(Fx_adminController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });

        dialogLayout.setBody(gridPane);
        //dialogLayout.setHeading(new Text("Login"));
        dialogLayout.setActions(button);
        
        dialog.show();
        dialog.setOnDialogClosed((JFXDialogEvent event1) ->{
            rootAnchorPaneAdmin.setEffect(null);
        });
        rootAnchorPaneAdmin.setEffect(blur);
    }


    private void makeFadeOut() {
        FadeTransition fadeTransition = new FadeTransition();
        fadeTransition.setDuration(Duration.millis(1000));
        fadeTransition.setNode(myStackPaneAdmin);
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);
        fadeTransition.setOnFinished((ActionEvent event)->{
            try {
                loadNextScene();
            } catch (IOException ex) {
                Logger.getLogger(Fx_adminController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        fadeTransition.play();
    }
    
    private void loadNextScene() throws IOException{
        Parent nextView;
        nextView = (StackPane)FXMLLoader.load(getClass().getResource("fx_main.fxml"));
        
        Scene newScence = new Scene(nextView);
        Stage curStage = (Stage) myStackPaneAdmin.getScene().getWindow();
        curStage.setScene(newScence);
    }
       
}
