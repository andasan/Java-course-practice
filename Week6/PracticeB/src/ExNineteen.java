import java.util.Scanner;

public class ExNineteen {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter an integer");
        int num = input.nextInt();

        int reversed = 0, remainder = 0;

        while(num != 0) {
            remainder = num % 10;
            reversed = reversed * 10 + remainder;
            num /= 10;
        }

        while (reversed > 0) {
            System.out.println(reversed%10);
            reversed = reversed / 10;
        }

    }
}
