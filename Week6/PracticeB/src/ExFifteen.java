import java.util.Scanner;
public class ExFifteen {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input request: ");
        char userInput = input.next().charAt(0);

        if ((userInput >= 65 && userInput <= 90) || (userInput >= 97 && userInput <= 122))
            System.out.println("Alphabet");
        else
            System.out.println("Not an alphabet");
    }


}
