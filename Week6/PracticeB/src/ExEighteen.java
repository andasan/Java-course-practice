import java.util.Scanner;

public class ExEighteen {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter your grade(s) and type 0 to end");
        int sum = 0, count = 0;
        int grade;
        double GPA;

        while((grade = input.nextInt()) != 0){
            sum += grade;
            count++;
        }
        GPA = sum/count;

        System.out.println("Your GPA is: "+GPA);

    }
}
