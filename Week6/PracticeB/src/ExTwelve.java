public class ExTwelve {
    public static void main(String[] args) {
        int rows = 4;
        int cols = 4;

        for(int i = 1; i <= rows; i++) {
            for (int j = cols; j >= 1; j--) {
                System.out.print("* ");
            }
            cols--;
            System.out.println();
        }
    }
}
