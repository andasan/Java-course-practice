public class ExSeventeen {
    public static void main(String[] args) {
        int rows = 3;
        int count = 1;

        for(int i = 1; i <= rows; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.printf("%2d", count);
                count++;
            }

            System.out.println();
        }
    }
}
